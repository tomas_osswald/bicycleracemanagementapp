package switchtwentytwenty.controllers;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.domain.dtos.input.AddStageDTO;
import switchtwentytwenty.domain.dtos.input.CreateRaceDTO;
import switchtwentytwenty.domain.model.Application;
import switchtwentytwenty.domain.utils.Result;

import java.sql.Date;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;

class AddStageControllerTest {

    Application application = new Application();

    // Race Info
    String teamName = "Sky";
    Date startDate = new Date(2021, 02, 01);
    Date endDate = new Date(2021, 02, 27);
    CreateRaceDTO createRaceDTO = new CreateRaceDTO(teamName, startDate, endDate);

    // Stage One Info
    Date validDate = new Date(2021, 02, 02);
    LocalTime startingTime = LocalTime.of(12, 30);
    String starLocation = "Covilhã";
    String finishLocation = "Guarda";
    Double distance = 40.00;

    /**
     * Add Stage Success - Stage successfully added
     */
    @Test
    void addStageSuccess() {
        CreateRaceController createRaceController = new CreateRaceController(application);
        createRaceController.createRace(createRaceDTO);

        AddStageDTO addStageDTO = new AddStageDTO(validDate, startingTime, starLocation, finishLocation, distance);
        AddStageController addStageController = new AddStageController(application);

        Result<String> expected = Result.success();
        Result<String> result = addStageController.addStage(addStageDTO);

        assertEquals(expected, result);
    }

    /**
     * Add Stage Unsuccess - Race Does not exists
     */
    @Test
    void addStageFailureRaceDoesNotExist() {
        AddStageDTO addStageDTO = new AddStageDTO(validDate, startingTime, starLocation, finishLocation, distance);
        AddStageController addStageController = new AddStageController(application);

        Result<String> expected = Result.failure("Race is not created yet!");
        Result<String> result = addStageController.addStage(addStageDTO);

        assertEquals(expected, result);
    }

    /**
     * Add Stage Unsuccess - Stage to be added is Out Of Bounds (Not between Race start and end date)
     */
    @Test
    void addStageFailureDateOutOfBoundaries() {
        CreateRaceController createRaceController = new CreateRaceController(application);
        createRaceController.createRace(createRaceDTO);

        Date invalidDate = new Date(2020, 02, 01);

        AddStageDTO addStageDTO = new AddStageDTO(invalidDate, startingTime, starLocation, finishLocation, distance);
        AddStageController addStageController = new AddStageController(application);

        Result<String> expected = Result.failure("Stage date must be inside race boundaries");
        Result<String> result = addStageController.addStage(addStageDTO);

        assertEquals(expected, result);
    }

    /**
     * Add Stage Unsuccess - There is already a stage that day
     */
    @Test
    void addStageFailureAnotherStageAlreadyExistsInThatDay() {
        CreateRaceController createRaceController = new CreateRaceController(application);
        createRaceController.createRace(createRaceDTO);

        AddStageDTO addStageDTO = new AddStageDTO(validDate, startingTime, starLocation, finishLocation, distance);
        AddStageController addStageController = new AddStageController(application);
        addStageController.addStage(addStageDTO);

        Result<String> expected = Result.failure("Already exists a stage in that day!");

        Result<String> result = addStageController.addStage(addStageDTO);

        assertEquals(expected, result);
    }

}