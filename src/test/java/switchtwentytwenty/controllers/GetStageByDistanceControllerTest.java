package switchtwentytwenty.controllers;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.domain.dtos.input.AddStageDTO;
import switchtwentytwenty.domain.dtos.input.CreateRaceDTO;
import switchtwentytwenty.domain.dtos.output.StageDTO;
import switchtwentytwenty.domain.model.Application;
import switchtwentytwenty.domain.utils.Result;

import java.sql.Date;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GetStageByDistanceControllerTest {

    Application application = new Application();
    CreateRaceController createRaceController = new CreateRaceController(application);
    AddStageController addStageController = new AddStageController(application);
    GetStageByDistanceController getStageByDistanceController = new GetStageByDistanceController(application);

    // Race Info
    String teamName = "Sky";
    Date startDate = new Date(2021, 02, 01);
    Date endDate = new Date(2021, 02, 27);
    CreateRaceDTO createRaceDTO = new CreateRaceDTO(teamName, startDate, endDate);


    // Stage One Info
    Date validDate = new Date(2021, 02, 02);
    LocalTime startingTime = LocalTime.of(12, 30);
    String starLocation = "Covilhã";
    String finishLocation = "Guarda";
    Double distance = 40.00;
    AddStageDTO addStageDTOOne = new AddStageDTO(validDate, startingTime, starLocation, finishLocation, distance);
    StageDTO stageDTO = new StageDTO(validDate, startingTime, starLocation, finishLocation, distance);


    // Stage Two Info

    Date validDateTwo = new Date(2021, 02, 03);
    LocalTime startingTimeTwo = LocalTime.of(12, 30);
    String starLocationTwo = "Guarda";
    String finishLocationTwo = "Viseu";
    Double distanceTwo = 60.00;
    AddStageDTO addStageDTOTwo = new AddStageDTO(validDateTwo, startingTimeTwo, starLocationTwo, finishLocationTwo, distanceTwo);
    StageDTO stageDTOTwo = new StageDTO(validDateTwo, startingTimeTwo, starLocationTwo, finishLocationTwo, distanceTwo);


    // Stage Three Info

    Date validDateThree = new Date(2021, 02, 04);
    LocalTime startingTimeThree = LocalTime.of(12, 30);
    String starLocationThree = "Viseu";
    String finishLocationThree = "Guarda";
    Double distanceThree = 60.00;
    AddStageDTO addStageDTOThree = new AddStageDTO(validDateThree, startingTimeThree, starLocationThree, finishLocationThree, distanceThree);
    StageDTO stageDTOThree = new StageDTO(validDateThree, startingTimeThree, starLocationThree, finishLocationThree, distanceThree);


    // Stage Four Info

    Date validDateFour = new Date(2021, 02, 04);
    LocalTime startingTimeFour = LocalTime.of(12, 30);
    String starLocationFour = "Guarda";
    String finishLocationFour = "Covilhã";
    Double distanceFour = 40.00;
    AddStageDTO addStageDTOFour = new AddStageDTO(validDateFour, startingTimeFour, starLocationFour, finishLocationFour, distanceFour);
    StageDTO stageDTOFour = new StageDTO(validDateFour, startingTimeFour, starLocationFour, finishLocationFour, distanceFour);


    @Test
    void getLongestDistanceStage() {
    }

    /**
     * Get Race Distance Success - One Stage
     */
    @Test
    void getRaceDistanceSuccessMoreThanZeroOneStage() {
        createRaceController.createRace(createRaceDTO);
        addStageController.addStage(addStageDTOOne); // 40.00

        Result<Double> expected = Result.success(40.00);

        Result<Double> result = getStageByDistanceController.getRaceDistance();

        assertEquals(expected, result);
    }

    /**
     * Get Race Distance Success - Two Stages
     */
    @Test
    void getRaceDistanceSuccessMoreThanZeroTwoStages() {
        createRaceController.createRace(createRaceDTO);
        addStageController.addStage(addStageDTOOne); // 40.00
        addStageController.addStage(addStageDTOTwo); // 60.00

        Result<Double> expected = Result.success(100.00);

        Result<Double> result = getStageByDistanceController.getRaceDistance();

        assertEquals(expected, result);
    }

    /**
     * Get Race Distance Success - No Stages
     */
    @Test
    void getRaceDistanceSuccessNoStages() {
        createRaceController.createRace(createRaceDTO);

        Result<Double> expected = Result.success(0.00);

        Result<Double> result = getStageByDistanceController.getRaceDistance();

        assertEquals(expected, result);
    }

    /**
     * Get Race Distance Unsuccess - No Race
     */
    @Test
    void getRaceDistanceUnsuccessNoRace() {
        Result<Double> expected = Result.failure("Race is not created yet!");

        Result<Double> result = getStageByDistanceController.getRaceDistance();

        assertEquals(expected, result);
    }

    /**
     * Get Shortest Distance Stage Success - One Stage in Race
     */
    @Test
    void getShortestDistanceStageSuccessOneStage() {
        createRaceController.createRace(createRaceDTO);
        addStageController.addStage(addStageDTOOne); // 40.00

        List<StageDTO> list = new ArrayList<>();
        list.add(stageDTO);

        Result<List<StageDTO>> expected = Result.success(list); // 40.00

        Result<List<StageDTO>> result = getStageByDistanceController.getShortestDistanceStage();

        assertEquals(expected, result);
    }

    /**
     * Get Shortest Distance Stage Success - Two Stages in Race, but only ONE with shortest distance
     */
    @Test
    void getShortestDistanceStageSuccessTwoStagesWithDiferentDistances() {
        createRaceController.createRace(createRaceDTO);
        addStageController.addStage(addStageDTOTwo); // 60.00
        addStageController.addStage(addStageDTOOne); // 40.00


        List<StageDTO> list = new ArrayList<>();
        list.add(stageDTO);

        Result<List<StageDTO>> expected = Result.success(list); // 40.00

        Result<List<StageDTO>> result = getStageByDistanceController.getShortestDistanceStage();

        assertEquals(expected, result);
    }

    /**
     * Get Shortest Distance Stage Success - Two Stages in Race, with same distance
     */
    @Test
    void getShortestDistanceStageSuccessTwoStagesWithSameDistances() {
        createRaceController.createRace(createRaceDTO);
        addStageController.addStage(addStageDTOThree); // 60.00
        addStageController.addStage(addStageDTOTwo); // 60.00

        List<StageDTO> list = new ArrayList<>();
        list.add(stageDTOThree); // 60.00
        list.add(stageDTOTwo); // 60.00

        Result<List<StageDTO>> expected = Result.success(list); // 60.00; 60.00

        Result<List<StageDTO>> result = getStageByDistanceController.getShortestDistanceStage();

        assertEquals(expected, result);
    }

    /**
     * Get Shortest Distance Stage Success - Three Stages in Race, TWO with shortest distance
     */
    @Test
    void getShortestDistanceStageSuccessThreeStagesWithTwoWithSameDistanceShortests() {
        createRaceController.createRace(createRaceDTO);
        addStageController.addStage(addStageDTOOne); // 40.00
        addStageController.addStage(addStageDTOTwo); // 60.00
        addStageController.addStage(addStageDTOFour); // 40.00

        List<StageDTO> list = new ArrayList<>();
        list.add(stageDTO); // 40.00
        list.add(stageDTOFour); // 40.00

        Result<List<StageDTO>> expected = Result.success(list); // 40.00 40.00

        Result<List<StageDTO>> result = getStageByDistanceController.getShortestDistanceStage();

        assertEquals(expected, result);
    }

    /**
     * Get Shortest Distance Stage Success - Three Stages in Race, only ONE with shortest distance
     */
    @Test
    void getShortestDistanceStageSuccessThreeStagesWithTwoWithOneShortests() {
        createRaceController.createRace(createRaceDTO);
        addStageController.addStage(addStageDTOOne); // 40.00
        addStageController.addStage(addStageDTOTwo); // 60.00
        addStageController.addStage(addStageDTOThree); // 60.00

        List<StageDTO> list = new ArrayList<>();
        list.add(stageDTO); // 40.00

        Result<List<StageDTO>> expected = Result.success(list); // 40.00

        Result<List<StageDTO>> result = getStageByDistanceController.getShortestDistanceStage();

        assertEquals(expected, result);
    }

    /**
     * Get Shortest Distance Stage Unsuccess - No Stages
     */
    @Test
    void getShortestDistanceStageUnsuccessNoStages() {
        createRaceController.createRace(createRaceDTO);
        Result<List<StageDTO>> expected = Result.failure("No stages in this race");

        Result<List<StageDTO>> result = getStageByDistanceController.getShortestDistanceStage();

        assertEquals(expected, result);
    }

    /**
     * Get Shortest Distance Stage Unsuccess - No Race
     */
    @Test
    void getShortestDistanceStageUnsuccessNoRace() {
        Result<List<StageDTO>> expected = Result.failure("Race is not created yet!");

        Result<List<StageDTO>> result = getStageByDistanceController.getShortestDistanceStage();

        assertEquals(expected, result);
    }
}