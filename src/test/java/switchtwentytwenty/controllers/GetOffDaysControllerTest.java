package switchtwentytwenty.controllers;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import switchtwentytwenty.domain.dtos.input.AddStageDTO;
import switchtwentytwenty.domain.dtos.input.CreateRaceDTO;
import switchtwentytwenty.domain.model.Application;
import switchtwentytwenty.domain.services.RaceService;
import switchtwentytwenty.domain.utils.Result;

import java.time.LocalTime;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

class GetOffDaysControllerTest {

    Application application = new Application();
    GetOffDaysController controller = new GetOffDaysController(application);

    String name = "Volta ao ISEP em Uniciclo";
    int year = 100;
    int january = 0;
    int dayOne = 1;
    int dayTen = 10;
    Date startDate = new Date(year, january, dayOne);
    Date endDate = new Date(year, january, dayTen);
    LocalTime localTime = LocalTime.of(1, 0);
    CreateRaceDTO singleDayRaceDTO = new CreateRaceDTO(name, startDate, startDate);
    CreateRaceDTO tenDayRaceDTO = new CreateRaceDTO(name, startDate, endDate);
    AddStageDTO addStageDTOOne = new AddStageDTO(startDate, localTime, "start", "finish", 1.00);

    RaceService raceService = new RaceService();

    /**
     * No race has been registered, so an exception will be thrown.
     */
    @Test
    void getOffDaysFailureNoRaceRegistered() {
        Result<List<Date>> expected = new Result<>(false, null, "Race is not created yet!");
        Result<List<Date>> result = controller.getOffDays();

        assertEquals(expected, result);
    }

    /**
     * Empty list is expected, because the race has no off days.
     */
    @Disabled
    @Test
    void getOffDaysSuccessEmptyList() {
        raceService.createRace(singleDayRaceDTO);
        raceService.addStage(addStageDTOOne);
        Result<List<Date>> expected = new Result<>(true, Collections.emptyList(), null);
        Result<List<Date>> result = controller.getOffDays();

        assertEquals(expected, result);
    }

}