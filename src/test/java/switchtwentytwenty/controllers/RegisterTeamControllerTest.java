package switchtwentytwenty.controllers;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import switchtwentytwenty.domain.dtos.input.AddRepresentativeDTO;
import switchtwentytwenty.domain.dtos.input.CreateTeamDTO;
import switchtwentytwenty.domain.model.Application;
import switchtwentytwenty.domain.model.Representative;
import switchtwentytwenty.domain.utils.Result;

import static org.junit.Assert.assertEquals;
@Disabled
class RegisterTeamControllerTest {
    Application app = new Application();
    RegisterTeamController controller = new RegisterTeamController(app);

    String teamName1 = "CrazyBirds";
    String representativeEmail = "some@gmail.com";
    String teamName2 = null;
    AddRepresentativeDTO addRepresentativeDTO = new AddRepresentativeDTO("TonyZe", "tonyze@mail.com");
    Representative representative = new Representative(addRepresentativeDTO);

    @Test
    void createTeamSuccessValidName() {
        CreateTeamDTO dto = new CreateTeamDTO(teamName1, representativeEmail);
        Result<String> expected = new Result<>(true, null, null);

        Result<String> result = controller.createTeam(dto);

        assertEquals(result, expected);
    }

    @Test
    void createTeamUnsuccessfulInvalidEmptyName() {
        String name = "";

        CreateTeamDTO createTeamDTO = new CreateTeamDTO(name, representativeEmail);
        Result<String> expected = new Result<>(false, null, "Name is empty or blank");

        Result<String> result = controller.createTeam(createTeamDTO);

        assertEquals(result, expected);
    }

}