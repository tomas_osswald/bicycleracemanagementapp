package switchtwentytwenty.controllers;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import switchtwentytwenty.domain.dtos.input.CreateRaceDTO;
import switchtwentytwenty.domain.model.Application;
import switchtwentytwenty.domain.utils.Result;

import java.util.Date;

import static org.junit.Assert.assertEquals;

class CreateRaceControllerTest {

    Application application = new Application();
    CreateRaceController controller = new CreateRaceController(application);

    String name = "Volta ao ISEP em Uniciclo";
    int year = 100;
    int january = 0;
    int february = 1;
    int dayOne = 1;
    int dayTen = 10;

    /**
     * Create a race with regular data input
     */
    @Test
    void createRaceSuccessful() {
        Date startDate = new Date(year, january, dayOne);
        Date endDate = new Date(year, january, dayTen);
        CreateRaceDTO createRaceDTO = new CreateRaceDTO(name, startDate, endDate);
        Result<String> expected = new Result<>(true, null, null);

        Result<String> result = controller.createRace(createRaceDTO);

        assertEquals(result, expected);
    }

    /**
     * Create a race with regular data input
     */
    @Disabled
    @Test
    void createRaceSuccessfulSameDate() {
        Date startDate = new Date(year, january, dayOne);
        Date endDate = new Date(year, january, dayOne);
        CreateRaceDTO createRaceDTO = new CreateRaceDTO(name, startDate, endDate);
        Result<String> expected = new Result<>(true, null, null);

        Result<String> result = controller.createRace(createRaceDTO);

        assertEquals(result, expected);
    }

    /**
     * Create a race with invalid (empty) name
     */
    @Test
    void createRaceUnsuccessfulInvalidEmptyName() {
        String name = "";
        Date startDate = new Date(year, january, dayOne);
        Date endDate = new Date(year, january, dayTen);
        CreateRaceDTO createRaceDTO = new CreateRaceDTO(name, startDate, endDate);
        Result<String> expected = new Result<>(false, null, "Name is empty or blank");

        Result<String> result = controller.createRace(createRaceDTO);

        assertEquals(result, expected);
    }

    /**
     * Create a race with invalid (blank) name
     */
    @Test
    void createRaceUnsuccessfulInvalidBlankName() {
        String name = "";
        Date startDate = new Date(year, january, dayOne);
        Date endDate = new Date(year, january, dayTen);
        CreateRaceDTO createRaceDTO = new CreateRaceDTO(name, startDate, endDate);
        Result<String> expected = new Result<>(false, null, "Name is empty or blank");

        Result<String> result = controller.createRace(createRaceDTO);

        assertEquals(result, expected);
    }

    /**
     * Create a race with invalid date (end date before start date)
     */
    @Test
    void createRaceUnsuccessfulInvalidDate() {
        Date startDate = new Date(year, february, dayOne);
        Date endDate = new Date(year, january, dayTen);
        CreateRaceDTO createRaceDTO = new CreateRaceDTO(name, startDate, endDate);
        Result<String> expected = new Result<>(false, null, "Date is invalid (end date before start date)");

        Result<String> result = controller.createRace(createRaceDTO);

        assertEquals(result, expected);
    }
}