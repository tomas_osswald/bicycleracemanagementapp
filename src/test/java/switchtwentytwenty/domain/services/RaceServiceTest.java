package switchtwentytwenty.domain.services;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.domain.dtos.input.CreateRaceDTO;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class RaceServiceTest {

    RaceService raceService = new RaceService();

    String name = "Volta ao ISEP em Uniciclo";
    int year = 100;
    int january = 0;
    int february = 1;
    int dayOne = 1;
    int dayTen = 10;

    /**
     * Create and record the instance of this race in the RaceService
     */
    @Test
    void createRaceSuccess() {
        Date startDate = new Date(year, january, dayOne);
        Date endDate = new Date(year, january, dayTen);
        CreateRaceDTO createRaceDTO = new CreateRaceDTO(name, startDate, endDate);

        assertTrue(raceService.createRace(createRaceDTO));
    }

    /**
     * Fail to create because a race already exists
     */
    @Test
    void createRaceFailure() {
        Date startDate = new Date(year, january, dayOne);
        Date endDate = new Date(year, january, dayTen);
        CreateRaceDTO createRaceDTO = new CreateRaceDTO(name, startDate, endDate);
        raceService.createRace(createRaceDTO);

        assertFalse(raceService.createRace(createRaceDTO));
    }


}