package switchtwentytwenty.domain.model;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.domain.dtos.input.AddStageDTO;
import java.sql.Date;
import java.time.LocalTime;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class StandardStageTest {

    // Stage One Info
    Date validDate = new Date(2021, 02, 02);
    LocalTime startingTime = LocalTime.of(12, 30);
    String starLocation = "Covilhã";
    String finishLocation = "Guarda";
    Double distance = 40.00;

    UUID uuid = UUID.randomUUID();

    AddStageDTO addStageDTO = new AddStageDTO(validDate, startingTime, starLocation, finishLocation, distance);

    /**
     * Create StandardStage Success - A valid instance is made
     */
    @Test
    void aValidStageInstance() {
        StandardStage standardStage = new StandardStage(addStageDTO, uuid);

        assertNotNull(standardStage);
    }

    /**
     * Create StandardStage Unsuccess - Invalid Distance (Zero)
     */
    @Test
    void failureInvalidDistanceZero() {
        Double invalidDistance = 0.00;

        AddStageDTO addStageDTOInvalid = new AddStageDTO(validDate, startingTime, starLocation, finishLocation, invalidDistance);

        assertThrows(IllegalArgumentException.class, () -> new StandardStage(addStageDTOInvalid, uuid));
    }

    /**
     * Create StandardStage Unsuccess - Invalid Distance (Null)
     */
    @Test
    void failureInvalidDistanceNull() {
        Double invalidDistance = 0.00;

        AddStageDTO addStageDTOInvalid = new AddStageDTO(validDate, startingTime, starLocation, finishLocation, invalidDistance);

        assertThrows(IllegalArgumentException.class, () -> new StandardStage(addStageDTOInvalid, uuid));
    }

    /**
     * Create StandardStage Unsuccess - Invalid Start Location (empty)
     */
    @Test
    void failureInvalidStartLocation() {
        String invalidLocation = "";

        AddStageDTO addStageDTOInvalid = new AddStageDTO(validDate, startingTime, invalidLocation, finishLocation, distance);

        assertThrows(IllegalArgumentException.class, () -> new StandardStage(addStageDTOInvalid, uuid));
    }

    /**
     * Create StandardStage Unsuccess - Invalid Finish Location (empty)
     */
    @Test
    void failureInvalidFinishLocation() {
        String invalidLocation = "";

        AddStageDTO addStageDTOInvalid = new AddStageDTO(validDate, startingTime, starLocation, invalidLocation, distance);

        assertThrows(IllegalArgumentException.class, () -> new StandardStage(addStageDTOInvalid, uuid));
    }

    /**
     * Create StandardStage Unsuccess - Invalid Starting Time (null)
     */
    @Test
    void failureInvalidStartingTime() {
        LocalTime invalidTime = null;

        AddStageDTO addStageDTOInvalid = new AddStageDTO(validDate, invalidTime, starLocation, finishLocation, distance);

        assertThrows(IllegalArgumentException.class, () -> new StandardStage(addStageDTOInvalid, uuid));
    }

    /**
     * Create StandardStage Unsuccess - Invalid Date (null)
     */
    @Test
    void failureInvalidDate() {
        Date invalidDate = null;

        AddStageDTO addStageDTOInvalid = new AddStageDTO(invalidDate, startingTime, starLocation, finishLocation, distance);

        assertThrows(IllegalArgumentException.class, () -> new StandardStage(addStageDTOInvalid, uuid));
    }
}