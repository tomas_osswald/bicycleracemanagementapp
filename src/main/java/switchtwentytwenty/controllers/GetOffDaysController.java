package switchtwentytwenty.controllers;

import switchtwentytwenty.domain.model.Application;
import switchtwentytwenty.domain.services.RaceService;
import switchtwentytwenty.domain.utils.Result;

import java.util.Date;
import java.util.List;

public class GetOffDaysController {

    private final Application application;

    public GetOffDaysController(Application application) {
        this.application = application;
    }

    public Result<List<Date>> getOffDays() {
        RaceService raceService = this.application.getRaceService();
        try {
            return Result.success(raceService.getOffDays());
        } catch (Exception e) {
            return Result.failure(e.getMessage());
        }

    }
}
