package switchtwentytwenty.controllers;

import switchtwentytwenty.domain.dtos.input.CreateTeamDTO;
import switchtwentytwenty.domain.model.Application;
import switchtwentytwenty.domain.services.RepresentativeService;
import switchtwentytwenty.domain.services.TeamService;
import switchtwentytwenty.domain.utils.Result;

public class RegisterTeamController {

    private final Application application;

    public RegisterTeamController(Application application) {
        this.application = application;
    }

    public Result<String> createTeam(CreateTeamDTO createTeamDTO) {
        TeamService teamService = this.application.getTeamService();

        Result<String> result;
        try {
            teamService.createTeam(createTeamDTO);
            return result = Result.success();
        } catch (Exception e) {
            return result = Result.failure(e.getMessage());
        }

    }
}

