package switchtwentytwenty.controllers;

import switchtwentytwenty.domain.dtos.input.RegisterAthleteDTO;
import switchtwentytwenty.domain.model.Application;
import switchtwentytwenty.domain.services.TeamService;
import switchtwentytwenty.domain.utils.Result;

public class RegisterAthleteController {

    private final Application application;

    public RegisterAthleteController(Application application) {
        this.application = application;
    }

    public Result<String> createAthlete(RegisterAthleteDTO registerAthleteDTO) {
        TeamService teamService = this.application.getTeamService();
        Result<String> result;
        try {
            teamService.registerAthlete(registerAthleteDTO);
            return result = Result.success();
        } catch (Exception e) {
            return result = Result.failure(e.getMessage());
        }

    }
}

