package switchtwentytwenty.controllers;

import switchtwentytwenty.domain.dtos.input.AddStageDTO;
import switchtwentytwenty.domain.model.Application;
import switchtwentytwenty.domain.services.RaceService;
import switchtwentytwenty.domain.utils.Result;

public class AddStageController {

    private Application application;

    public AddStageController(Application application) {
        this.application = application;
    }

    /**
     * Method to Add a Stage
     * @param addStageDTO Input DTO with necessary data
     * @return Result objetct with boolean (true if a stage as been successfully added, false if an error has occurred)
     */
    public Result<String> addStage(AddStageDTO addStageDTO) {
        RaceService raceService = application.getRaceService();
        Result<String> result;
        try {
            raceService.addStage(addStageDTO);
            result = Result.success();
        } catch (Exception e) {
            result = Result.failure(e.getMessage());
        }
        return result;
    }
}
