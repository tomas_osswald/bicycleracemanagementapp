package switchtwentytwenty.controllers;


import switchtwentytwenty.domain.dtos.output.StageDTO;

import switchtwentytwenty.domain.model.Application;
import switchtwentytwenty.domain.model.Race;
import switchtwentytwenty.domain.services.RaceService;
import switchtwentytwenty.domain.utils.Result;

import java.util.List;

public class GetStageByDistanceController {

    private final Application application;

    public GetStageByDistanceController(Application application) {
        this.application = application;
    }

    public Result<List<StageDTO>> getLongestDistanceStage() {
        RaceService raceService = this.application.getRaceService();
        try {
            return Result.success(raceService.getLongestDistanceStage());
        } catch (Exception e) {
            return Result.failure(e.getMessage());
        }
    }

    /**
     * Method to get Total Race Distance
     *
     * @return Result object with distance (for a success case), or a Result object with an error message (for a unsuccess case)
     */
    public Result<Double> getRaceDistance() {
        RaceService raceService = application.getRaceService();
        try {
            Double result = raceService.getRaceDistance();
            return Result.success(result);
        } catch (Exception e) {
            return Result.failure(e.getMessage());
        }
    }

    /**
     * Method to get List of Stages with shortest distance in Race
     *
     * @return Result object with List of Stages with shortest distance (for a success case), or a Result object with an
     * error message (for a unsuccess case)
     */
    public Result<List<StageDTO>> getShortestDistanceStage() {
        RaceService raceService = application.getRaceService();
        try {
            List<StageDTO> result = raceService.getShortestDistanceStage();
            return Result.success(result);
        } catch (Exception e) {
            return Result.failure(e.getMessage());
        }
    }
}