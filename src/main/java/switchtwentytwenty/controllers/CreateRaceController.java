package switchtwentytwenty.controllers;

import switchtwentytwenty.domain.dtos.input.CreateRaceDTO;
import switchtwentytwenty.domain.model.Application;
import switchtwentytwenty.domain.services.RaceService;
import switchtwentytwenty.domain.utils.Result;

public class CreateRaceController {

    private final Application application;

    public CreateRaceController(Application application) {
        this.application = application;
    }

    public Result<String> createRace(CreateRaceDTO createRaceDTO) {
        RaceService raceService = this.application.getRaceService();
        Result<String> result;
        try {
            raceService.createRace(createRaceDTO);
            return result = Result.success();
        } catch (Exception e) {
            return result = Result.failure(e.getMessage());
        }

    }
}
