package switchtwentytwenty.domain.utils;

import java.util.Objects;

public class Result<T> {

    private final boolean aBoolean;
    private final T content;
    private final String message;

    public Result(boolean result, T content, String message) {
        this.aBoolean = result;
        this.content = content;
        this.message = message;
    }

    public static <T> Result<T> success() {
        return new Result<>(true, null, null);
    }

    public static <T> Result<T> failure(String messageContent) {
        return new Result<>(false, null, messageContent);
    }

    public static <T> Result<T> success(T contentObject) {
        return new Result<>(true, contentObject, null);
    }

    public boolean isSuccess() {
        return aBoolean;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Result)) return false;

        Result<T> otherResult = (Result<T>) o;

        boolean equals = true;

        if (this.aBoolean != otherResult.aBoolean)
            return false;

        if (this.content != null && otherResult.content != null) {
            equals = this.content.equals(otherResult.content);
        } else if (this.content != null || otherResult.content != null)
            return false;

        if (this.message != null && otherResult.message != null) {
            equals = this.message.equals(otherResult.message);
        } else if (this.message != null || otherResult.message != null)
            return false;

        return equals;
    }

    @Override
    public int hashCode() {
        return Objects.hash(aBoolean, content);
    }
}

