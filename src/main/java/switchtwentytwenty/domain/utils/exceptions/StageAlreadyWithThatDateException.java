package switchtwentytwenty.domain.utils.exceptions;

public class StageAlreadyWithThatDateException extends RuntimeException {

    public StageAlreadyWithThatDateException(String message) {
        super(message);
    }

    public StageAlreadyWithThatDateException() {

    }
}
