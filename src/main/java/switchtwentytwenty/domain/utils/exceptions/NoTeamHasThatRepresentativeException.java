package switchtwentytwenty.domain.utils.exceptions;

public class NoTeamHasThatRepresentativeException extends RuntimeException{

    public NoTeamHasThatRepresentativeException(String error) {
        super(error);
    }

    public NoTeamHasThatRepresentativeException() {
    }
}
