package switchtwentytwenty.domain.utils.exceptions;

public class RaceNotPresentException extends RuntimeException {

    public RaceNotPresentException(String message) {
        super(message);
    }

    public RaceNotPresentException() {

    }
}
