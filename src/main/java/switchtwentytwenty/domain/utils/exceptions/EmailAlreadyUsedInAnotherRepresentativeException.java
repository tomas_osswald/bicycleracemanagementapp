package switchtwentytwenty.domain.utils.exceptions;

public class EmailAlreadyUsedInAnotherRepresentativeException extends RuntimeException{

    public EmailAlreadyUsedInAnotherRepresentativeException(String message) {
        super(message);
    }

    public EmailAlreadyUsedInAnotherRepresentativeException() {

    }

}
