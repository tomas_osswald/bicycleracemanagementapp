package switchtwentytwenty.domain.utils.exceptions;

public class DateOutOfBoundariesException extends RuntimeException {

    public DateOutOfBoundariesException(String message) {
        super(message);
    }

    public DateOutOfBoundariesException() {

    }
}
