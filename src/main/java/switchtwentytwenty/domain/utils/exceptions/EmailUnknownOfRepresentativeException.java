package switchtwentytwenty.domain.utils.exceptions;

public class EmailUnknownOfRepresentativeException extends RuntimeException{

    public EmailUnknownOfRepresentativeException(String message) {
        super(message);
    }

    public EmailUnknownOfRepresentativeException() {

    }
}
