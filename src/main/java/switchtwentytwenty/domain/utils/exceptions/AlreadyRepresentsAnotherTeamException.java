package switchtwentytwenty.domain.utils.exceptions;

public class AlreadyRepresentsAnotherTeamException extends RuntimeException{

    public AlreadyRepresentsAnotherTeamException(String message) {
        super(message);
    }

    public AlreadyRepresentsAnotherTeamException() {

    }
}
