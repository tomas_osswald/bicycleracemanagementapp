package switchtwentytwenty.domain.utils.exceptions;

public class NameOfTeamIsAlreadyOwnedException extends RuntimeException{

    public NameOfTeamIsAlreadyOwnedException (String message) {
        super(message);
    }

    public NameOfTeamIsAlreadyOwnedException() {

    }
}
