package switchtwentytwenty.domain.utils.exceptions;

public class NameOfNoTeamException extends RuntimeException {

    public NameOfNoTeamException(String message) {
        super(message);
    }

    public NameOfNoTeamException() {

    }
}
