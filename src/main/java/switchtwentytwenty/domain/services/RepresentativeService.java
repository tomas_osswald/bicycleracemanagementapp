package switchtwentytwenty.domain.services;

import switchtwentytwenty.domain.dtos.input.AddRepresentativeDTO;
import switchtwentytwenty.domain.model.Representative;
import switchtwentytwenty.domain.utils.exceptions.EmailAlreadyUsedInAnotherRepresentativeException;
import switchtwentytwenty.domain.utils.exceptions.EmailUnknownOfRepresentativeException;

import java.util.List;

public class RepresentativeService {

    private List<Representative> representativeList;

    public RepresentativeService() {

    }

    public Representative getRepresentativeByEmail(String representativeEmail) {
        Representative representative = null;
        for (int i = 0; i < representativeList.size(); i++) {
            if (representativeList.get(i).isThisMyEmail(representativeEmail)) {
                representative = representativeList.get(i);
            }
        }
        if (representative == null) {
            throw new EmailUnknownOfRepresentativeException("The email is of no Representative");
        }
        return representative;
    }

    private void checkIfEmailAlreadyPresent(String representativeEmail) {
        for (int i = 0; i < representativeList.size(); i++) {
            if (representativeList.get(i).isThisMyEmail(representativeEmail)) {
                throw new EmailAlreadyUsedInAnotherRepresentativeException("This email is used by another representative");

            }
        }
    }

    private void addRepresentativeToList(Representative representative) {
        representativeList.add(representative);
    }

    public void createRepresentative(AddRepresentativeDTO addRepresentativeDTO) {
        checkIfEmailAlreadyPresent(addRepresentativeDTO.getRepresentativeEmail());
        Representative newRepresentative = new Representative(addRepresentativeDTO);
        addRepresentativeToList(newRepresentative);
    }


}







