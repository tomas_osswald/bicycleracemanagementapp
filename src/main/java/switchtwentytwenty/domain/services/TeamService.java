package switchtwentytwenty.domain.services;

import switchtwentytwenty.domain.dtos.input.RegisterAthleteDTO;
import switchtwentytwenty.domain.dtos.input.CreateTeamDTO;
import switchtwentytwenty.domain.model.Representative;
import switchtwentytwenty.domain.model.Team;
import switchtwentytwenty.domain.utils.exceptions.AlreadyRepresentsAnotherTeamException;
import switchtwentytwenty.domain.utils.exceptions.NameOfTeamIsAlreadyOwnedException;
import switchtwentytwenty.domain.utils.exceptions.NoTeamHasThatRepresentativeException;

import java.util.List;

public class TeamService {

    private RepresentativeService representativeService;
    private List<Team> teams;

    public TeamService(RepresentativeService representativeService) {
        this.representativeService = representativeService;
    }

    public void checkRepresentativeOfAnyTeam(Representative representative) {
        for (int i = 0; i < teams.size(); i++) {
            if (teams.get(i).isYourRepresentative(representative)) {
                throw new AlreadyRepresentsAnotherTeamException("Already represents another team");
            }
        }
    }

    public void checkNameOfAnyTeam(String teamName) {
        for (int i = 0; i < teams.size(); i++) {
            if (teams.get(i).isThisYourName(teamName)) {
                throw new NameOfTeamIsAlreadyOwnedException("Name of another team");
            }
        }
    }

    public void createTeam(CreateTeamDTO createTeamDTO) {
        String teamName = createTeamDTO.getTeamName();
        String representativeEmail = createTeamDTO.getRepresentativeEmail();
        Representative representative = representativeService.getRepresentativeByEmail(representativeEmail);
        checkRepresentativeOfAnyTeam(representative);
        checkNameOfAnyTeam(teamName);
        Team newTeam = new Team(createTeamDTO, representative);
        addTeamToList(newTeam);

    }

    private void addTeamToList(Team team) {
        teams.add(team);
    }

    public Team getTeamByRepresentative(Representative representative) {
        Team nTeam = null;
        for (int i = 0; i < teams.size(); i++) {
            if(teams.get(i).isYourRepresentative(representative)){
                nTeam = teams.get(i);
            }
        }
        if (nTeam == null) {
            throw new NoTeamHasThatRepresentativeException("There is no team represented by that Representative");
        }
        return nTeam;
    }

    public void registerAthlete(RegisterAthleteDTO registerAthleteDTO) {
        String representativeEmail = registerAthleteDTO.getRepresentativeEmail();
        Representative representative = representativeService.getRepresentativeByEmail(representativeEmail);
        Team aTeam = getTeamByRepresentative(representative);
        aTeam.registerAthlete(registerAthleteDTO);
    }
}

