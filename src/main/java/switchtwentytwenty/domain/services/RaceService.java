package switchtwentytwenty.domain.services;


import switchtwentytwenty.domain.dtos.input.AddStageDTO;
import switchtwentytwenty.domain.dtos.input.CreateRaceDTO;
import switchtwentytwenty.domain.dtos.output.StageDTO;
import switchtwentytwenty.domain.model.Race;
import switchtwentytwenty.domain.utils.exceptions.RaceNotPresentException;

import java.util.Date;
import java.util.List;

public class RaceService {

    private Race race;

    public RaceService() {
    }

    public boolean createRace(CreateRaceDTO createRaceDTO) {
        boolean success = false;
        if (this.race == null) {
            this.race = new Race(createRaceDTO);
            success = true;
        }
        return success;
    }

    /**
     * Method to forward data to race to add a stage
     * Checks if a race is present
     *
     * @param addStageDTO DTO with necessary data
     * @return true
     */
    public boolean addStage(AddStageDTO addStageDTO) {
        checkRace();
        this.race.addStage(addStageDTO);
        return true;
    }

    public List<Date> getOffDays() {
        checkRace(); // Meti a verificação noutro metodo para ser utilizada em mais lados ASS: Johnny Sins
        return this.race.getOffDays();
    }

    public List<StageDTO> getLongestDistanceStage() {
        return this.race.getLongestDistanceStage();
    }

    /**
     * Method to get Total Race Distance
     * Checks if a race is present
     *
     * @return Double with Total Distance
     */
    public Double getRaceDistance() {
        checkRace();
        return this.race.getRaceDistance();
    }

    /**
     * Method to get List of Stages with shortest distance in Race
     * Checks if a race is present
     *
     * @return List of StageDTO
     */
    public List<StageDTO> getShortestDistanceStage() {
        checkRace();
        return this.race.getShortestDistanceStage();
    }

    /**
     * Method to verify if a Race is already presente in system
     */
    private void checkRace() {
        if (this.race == null)
            throw new RaceNotPresentException("Race is not created yet!");
    }
}
