package switchtwentytwenty.domain.dtos.input;

public class AddRepresentativeDTO {

    private final String representativeName;
    private final String representativeEmail;

    public AddRepresentativeDTO(String representativeName, String representativeEmail) {
        this.representativeName = representativeName;
        this.representativeEmail = representativeEmail;
    }

    public String getRepresentativeName() {
        return representativeName;
    }

    public String getRepresentativeEmail() {
        return representativeEmail;
    }

}
