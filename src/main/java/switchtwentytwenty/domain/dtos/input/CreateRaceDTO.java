package switchtwentytwenty.domain.dtos.input;

import java.util.Date;

public class CreateRaceDTO {

    private final String name;
    private final Date startDate;
    private final Date endDate;

    public CreateRaceDTO(String name, Date startDate, Date endDate) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

}
