package switchtwentytwenty.domain.dtos.input;

import switchtwentytwenty.domain.model.Representative;

public class CreateTeamDTO {

    private final String teamName;
    private final String representativeEmail;


    public CreateTeamDTO(String teamName, String representativeEmail) {
        this.teamName = teamName;
        this.representativeEmail = representativeEmail;
    }

    public String getTeamName() {
        return teamName;
    }

    public String getRepresentativeEmail() {
        return representativeEmail;
    }
}

