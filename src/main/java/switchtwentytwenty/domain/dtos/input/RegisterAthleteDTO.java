package switchtwentytwenty.domain.dtos.input;

import java.util.Date;

public class RegisterAthleteDTO {

    public String getRepresentativeEmail() {
        return representativeEmail;
    }

    public PersonDataDTO getPersonDataDTO() {
        return personDataDTO;
    }

    public AthleteDataDTO getAthleteDataDTO() {
        return athleteDataDTO;
    }

    private String representativeEmail;
    private PersonDataDTO personDataDTO;
    private AthleteDataDTO athleteDataDTO;

    public RegisterAthleteDTO(String athleteName, Date birthDate, double height, double weight, String UCINumber, String representativeEmail) {
        this.representativeEmail = representativeEmail;
        this.personDataDTO = new PersonDataDTO(athleteName, birthDate);
        this.athleteDataDTO = new AthleteDataDTO(UCINumber, height, weight);
       }

}
