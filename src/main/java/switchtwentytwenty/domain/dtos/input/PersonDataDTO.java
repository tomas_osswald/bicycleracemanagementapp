package switchtwentytwenty.domain.dtos.input;

import java.util.Date;

public class PersonDataDTO {

    public String getPersonName() {
        return personName;
    }

    public Date getBirthDatePerson() {
        return birthDatePerson;
    }

    private String personName;
    private Date birthDatePerson;

    public PersonDataDTO(String personName, Date birthDatePerson) {
        this.personName = personName;
        this.birthDatePerson = birthDatePerson;
    }

}
