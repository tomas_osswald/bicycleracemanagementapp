package switchtwentytwenty.domain.dtos.input;

import java.time.LocalTime;
import java.util.Date;

public class AddStageDTO {

    private Date date;
    private LocalTime startingTime;
    private String startLocation;
    private String finishLocation;
    private Double distance;

    public AddStageDTO(Date date, LocalTime startingTime, String startLocation, String finishLocation, Double distance) {
        this.date = date;
        this.startingTime = startingTime;
        this.startLocation = startLocation;
        this.finishLocation = finishLocation;
        this.distance = distance;
    }

    public Date getDate() {
        return this.date;
    }

    public LocalTime getStartingTime() {
        return startingTime;
    }

    public String getStartLocation() {
        return startLocation;
    }

    public String getFinishLocation() {
        return finishLocation;
    }

    public Double getDistance() {
        return distance;
    }
}
