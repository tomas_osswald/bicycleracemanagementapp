package switchtwentytwenty.domain.dtos.input;


public class AthleteDataDTO {

    public String getUCINumber() {
        return UCINumber;
    }

    public double getHeight() {
        return height;
    }

    public double getWeight() {
        return weight;
    }

    private String UCINumber;
    private double height;
    private double weight;

    public AthleteDataDTO(String UCINumber, double height, double weight) {
        this.UCINumber = UCINumber;
        this.height = height;
        this.weight = weight;
    }


}
