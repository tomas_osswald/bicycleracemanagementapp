package switchtwentytwenty.domain.dtos.output;

import java.time.LocalTime;
import java.util.Date;
import java.util.Objects;

public class StageDTO {

    private Date date;
    private LocalTime startingTime;
    private String startLocation;
    private String finishLocation;
    private Double distance;

    public StageDTO(Date date, LocalTime startingTime, String startLocation, String finishLocation, Double distance) {
        this.date = date;
        this.startingTime = startingTime;
        this.startLocation = startLocation;
        this.finishLocation = finishLocation;
        this.distance = distance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StageDTO)) return false;
        StageDTO stageDTO = (StageDTO) o;
        return Objects.equals(date, stageDTO.date) &&
                Objects.equals(startingTime, stageDTO.startingTime) &&
                Objects.equals(startLocation, stageDTO.startLocation) &&
                Objects.equals(finishLocation, stageDTO.finishLocation) &&
                Objects.equals(distance, stageDTO.distance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, startingTime, startLocation, finishLocation, distance);
    }



}
