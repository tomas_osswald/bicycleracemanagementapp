package switchtwentytwenty.domain.model;

import switchtwentytwenty.domain.dtos.input.AthleteDataDTO;
import switchtwentytwenty.domain.dtos.input.PersonDataDTO;

public class Cyclist implements Athlete {
    private PersonData personData;
    private AthleteData athleteData;

    public Cyclist(PersonDataDTO personDataDTO, AthleteDataDTO athleteDataDTO) {
        this.personData = new PersonData(personDataDTO);
        this.athleteData = new AthleteData(athleteDataDTO);
    }

}
