package switchtwentytwenty.domain.model;


import switchtwentytwenty.domain.dtos.input.AddStageDTO;
import switchtwentytwenty.domain.dtos.input.CreateRaceDTO;
import switchtwentytwenty.domain.dtos.output.StageDTO;
import switchtwentytwenty.domain.utils.exceptions.DateOutOfBoundariesException;

import java.util.Date;
import java.util.List;

public class Race {

    private final String name;
    private final Date startDate;
    private final Date endDate;
    private final StageList stageList = new StageList();
    private Standings generalStandings;


    public Race(CreateRaceDTO createRaceDTO) {
        String newName = createRaceDTO.getName();
        if (!isNameValid(newName)) {
            throw new IllegalArgumentException("Name is empty or blank");
        }
        Date newStartDate = createRaceDTO.getStartDate();
        Date newEndDate = createRaceDTO.getEndDate();
        if (!isDateValid(newStartDate, newEndDate)) {
            throw new IllegalArgumentException("Date is invalid (end date before start date)");
        }
        this.name = newName;
        this.startDate = newStartDate;
        this.endDate = newEndDate;

    }

    /**
     * Method to forward data to race to add a stage
     * Checks if date is between race start and end date
     *
     * @param addStageDTO DTO with necessary data
     * @return true
     */
    public boolean addStage(AddStageDTO addStageDTO) {
        verifyDate(addStageDTO.getDate());
        this.stageList.addStage(addStageDTO);
        return true;
    }

    /**
     * Method to verify if a given date is between race start and end date
     *
     * @param date date to verify
     */
    private void verifyDate(Date date) {
        if (date.before(startDate) || date.after(endDate))
            throw new DateOutOfBoundariesException("Stage date must be inside race boundaries");
    }

    private boolean isDateValid(Date startDate, Date endDate) {
        boolean isValid = true;
        if (endDate.before(startDate) || endDate.equals(startDate)) {
            isValid = false;
        }
        return isValid;
    }

    private boolean isNameValid(String name) {
        boolean isValid = true;
        if (name.isEmpty()) {
            isValid = false;
        }
        return isValid;
    }

    public List<Date> getOffDays() {
        return stageList.getOffDays(getRaceDays());
    }

    /**
     * A method that returns the list of days of the race
     *
     * @return list of days of this race
     */
    private List<Date> getRaceDays() {
        List<Date> raceDays = null;
        Date date;
        for (date = startDate; date.equals(endDate); date.setDate(date.getDate() + 1)) {
            raceDays.add(date);
        }
        return raceDays;
    }

    public List<StageDTO> getLongestDistanceStage() {
        return this.stageList.getLongestDistanceStage();
    }

    /**
     * Method to get Total Race Distance
     *
     * @return Double with Total Distance
     */
    public Double getRaceDistance() {
        return this.stageList.getRaceDistance();
    }

    /**
     * Method to get List of Stages with shortest distance in Race
     *
     * @return List of StageDTO
     */
    public List<StageDTO> getShortestDistanceStage() {
        return this.stageList.getShortestDistamceStage();
    }
}
