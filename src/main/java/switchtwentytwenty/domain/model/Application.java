package switchtwentytwenty.domain.model;

import switchtwentytwenty.domain.services.*;


public class Application {

    private final RaceService raceService = new RaceService();

    private final RepresentativeService representativeService = new RepresentativeService();
    private final TeamService teamService = new TeamService(representativeService);
    public Application(){

    }

    public RaceService getRaceService() {
        return raceService;
    }

    public TeamService getTeamService() {
        return teamService;
    }
}
