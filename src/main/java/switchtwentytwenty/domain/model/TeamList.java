package switchtwentytwenty.domain.model;

import switchtwentytwenty.domain.utils.exceptions.EmailAlreadyUsedInAnotherRepresentativeException;
import switchtwentytwenty.domain.utils.exceptions.NameOfNoTeamException;
import switchtwentytwenty.domain.utils.exceptions.NameOfTeamIsAlreadyOwnedException;

import java.util.ArrayList;
import java.util.List;

public class TeamList {



    public List<Team> getTeams() {
        return teams;
    }

    public Team getTeam() {
        return team;
    }

    private Team team;
    private final List<Team> teams = new ArrayList<>();

    private boolean verifiyTeamNameisUnique(String teamName) {
        for (int i = 0; i < teams.size(); i++) {
            if (teamName == teams.get(i).getTeamName()) {
                throw new NameOfTeamIsAlreadyOwnedException("This name is used by another team");
            }
        }
        return true;
    }

    public boolean checkIfNameOfAnyTeam(String teamName) {
        for (int i = 0; i < teams.size(); i++) {
            if (teamName == teams.get(i).getTeamName()) {
                throw new NameOfNoTeamException("Name of any team");
            }
        }
        return true;
    }

    public Team getTeam(String teamName) {
        for (int i = 0; i < teams.size(); i++) {
            if(checkIfNameOfAnyTeam(teamName)){
                Team team = this.teams.get(i);
        }
    }
    return team;
}

    private void addTeam(Team team) {
        this.teams.add(team);
    }
}
