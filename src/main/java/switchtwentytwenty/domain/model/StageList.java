package switchtwentytwenty.domain.model;

import switchtwentytwenty.domain.dtos.input.AddStageDTO;
import switchtwentytwenty.domain.dtos.output.StageDTO;
import switchtwentytwenty.domain.utils.exceptions.StageAlreadyWithThatDateException;


import java.util.*;

public class StageList {

    private final List<Stage> stages = new ArrayList<>();

    /**
     * Method to add a given Stage to List of Stages
     *
     * @param stage Stage to add
     */
    private void addStage(Stage stage) {
        this.stages.add(stage);
    }

    /**
     * Method add a Stage
     * Checks if is already a Stage in a given date
     * Create a new StandardStage with addStageDTO
     * Add that instance to list of stages
     *
     * @param addStageDTO DTO with necessary data
     * @return true
     */
    public boolean addStage(AddStageDTO addStageDTO) {
        verifyDate(addStageDTO.getDate());
        UUID uuid = generateID();

        Stage standardStage = new StandardStage(addStageDTO, uuid);
        addStage(standardStage);
        return true;
    }

    /**
     * Method to generate and UUID for a stage
     * Generates an UUID and than verify if a Stage already have that UUID
     * If not present, returns that UUID
     * If present, generates a new UUID and chek for its presence again
     *
     * @return UUID
     */
    private UUID generateID() {
        UUID uuid;
        boolean uuidExists;

        do {
            uuidExists = false;
            uuid = UUID.randomUUID();
            for (int i = 0; i < stages.size() && !uuidExists; i++) {
                if (stages.get(i).isMyUUID(uuid)) {
                    uuidExists = true;
                }
            }
        } while (uuidExists);

        return uuid;
    }

    /**
     * Method to verify if is already a Stage in a given date
     *
     * @param date Date to verify
     */
    private void verifyDate(Date date) {
        for (Stage stage : stages) {
            if (stage.isMyDate(date)) {
                throw new StageAlreadyWithThatDateException("Already exists a stage in that day!");
            }
        }
    }

    /**
     * A method that returns a list of days from a race where there are no stages.
     *
     * @param raceDays list of days from the race
     * @return list of off days
     */
    public List<Date> getOffDays(List<Date> raceDays) {
        List<Date> offDays = Collections.emptyList();

        for (Date date : raceDays) {
            boolean isOffDay = true;

            for (Stage stage : stages) {
                if (stage.isMyDate(date)) {
                    isOffDay = false;
                }
            }

            if (isOffDay == true) {
                offDays.add(date);
            }
        }
        return offDays;
    }

    public List<StageDTO> getLongestDistanceStage() {
        checkForStagePresence();
        Stage longest = stages.get(0);
        List<Stage> longestList = new ArrayList<>();
        longestList.add(longest);

        if (stages.size() >= 2) {
            for (int i = 1; i < stages.size(); i++) {
                int comparison = longest.compareDistance(stages.get(i));
                if (comparison == -1) {
                    longest = stages.get(i);
                    longestList.clear();
                    longestList.add(longest);
                } else if (comparison == 0) {
                    longestList.add(stages.get(i));
                }
            }
        }
        return createDTOList(longestList);
    }

    /**
     * Method to for each stage in a given list, create a new List of StageDTOs of stages
     *
     * @param longestList Given Stage List
     * @return List of StageDTO
     */
    private List<StageDTO> createDTOList(List<Stage> longestList) {
        List<StageDTO> list = new ArrayList();

        for (Stage stage : longestList) {
            list.add(stage.getStageDTO());
        }
        return list;
    }

    /**
     * Method to get Total Race Distance
     * For each stage in stages, sum each distance to total Distance
     * If no Stages are present, (Double) 0.00 is return
     *
     * @return Double with Total Distance
     */
    public Double getRaceDistance() {
        Double distance = 0.00;

        for (Stage stage : stages) {
            distance += stage.getDistance();
        }
        return distance;
    }

    /**
     * Method to get List of Stages with shortest distance in Race
     * Check if exists Stages in Stage List
     *
     * @return List of StageDTO
     */
    public List<StageDTO> getShortestDistamceStage() {
        checkForStagePresence();

        Stage longest = stages.get(0);
        List<Stage> longestList = new ArrayList<>();
        longestList.add(longest);

        if (stages.size() >= 2) {
            for (int i = 1; i < stages.size(); i++) {
                int comparison = longest.compareDistance(stages.get(i));
                if (comparison == 1) { // if equals One (1) stages.get(i) is shorter than longest
                    longest = stages.get(i);
                    longestList.clear();
                    longestList.add(longest);
                } else if (comparison == 0) {
                    longestList.add(stages.get(i));
                }
            }
        }
        return createDTOList(longestList);
    }

    /**
     * Method to check if exists Stages in Stage List
     * Throws an Exception
     */
    private void checkForStagePresence() {
        if (stages.isEmpty()) {
            throw new IllegalArgumentException("No stages in this race");
        }
    }

    public void sortByDistance() {
        this.stages.sort(new Comparator<Stage>() {
            @Override
            public int compare(Stage o1, Stage o2) {
                if (o1.getDistance() < o2.getDistance()){
                    return -1;
                }else if (o1.getDistance() > o2.getDistance()) {
                    return 1;
                }
                else return 0;
            }
        });
    }
}
