package switchtwentytwenty.domain.model;

import switchtwentytwenty.domain.dtos.output.StageDTO;

import java.util.Date;
import java.util.UUID;


public interface Stage {

    public boolean isMyDate(Date date);

    boolean isMyUUID(UUID uuid);

    public int compareDistance(Stage stage);

    public StageDTO getStageDTO();

    public Double getDistance();
}
