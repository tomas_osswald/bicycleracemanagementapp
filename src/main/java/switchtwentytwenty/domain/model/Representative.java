package switchtwentytwenty.domain.model;

import switchtwentytwenty.domain.dtos.input.AddRepresentativeDTO;

public class Representative {

    private final EmailAddress representativeEmail;
    private final String representativeName;

    public Representative(AddRepresentativeDTO addRepresentativeDTO) {
        this.representativeName = addRepresentativeDTO.getRepresentativeName();
        this.representativeEmail = new EmailAddress(addRepresentativeDTO.getRepresentativeEmail());
    }

    public boolean isThisMyEmail(String representativeEmail) {
        boolean isThisMyEmail = false;
        if(this.representativeEmail.compareEmailByString(representativeEmail)){
            isThisMyEmail = true;
        }
       return isThisMyEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Representative)) return false;
        Representative that = (Representative) o;
        return this.representativeEmail.equals(that.representativeEmail);
    }

}
