package switchtwentytwenty.domain.model;

import switchtwentytwenty.domain.dtos.input.AddStageDTO;
import switchtwentytwenty.domain.dtos.output.StageDTO;


import java.time.LocalTime;
import java.util.Date;
import java.util.UUID;

public class StandardStage implements Stage {

    private final UUID id;
    private Date date;
    private LocalTime startingTime;
    private String startLocation;
    private String finishLocation;
    private Double distance;

    public StandardStage(AddStageDTO addStageDTO, UUID id) {
        this.id = id;
        if (isValidDate(addStageDTO.getDate())) {
            this.date = addStageDTO.getDate();
        }

        if (isValidStartingTime(addStageDTO.getStartingTime())) {
            this.startingTime = addStageDTO.getStartingTime();
        }

        if (isValidLocation(addStageDTO.getStartLocation())) {
            this.startLocation = addStageDTO.getStartLocation();
        } else {
            throw new IllegalArgumentException("Starting Location must not null and not empty");
        }

        if (isValidLocation(addStageDTO.getFinishLocation())) {
            this.finishLocation = addStageDTO.getFinishLocation();
        } else {
            throw new IllegalArgumentException("Finish Location must not null and not empty");
        }

        if (isValidDistance(addStageDTO.getDistance())) {
            this.distance = addStageDTO.getDistance();
        }

    }

    /**
     * Method to verify if Distance is greater than zero
     * If distance less or equals than zero throws an IllegalArgumentException
     *
     * @param distance distance to verify
     * @return true if is a valid Distance
     */
    private boolean isValidDistance(Double distance) {
        if (distance <= 0) {
            throw new IllegalArgumentException("Distance must be Greater than Zero");
        }
        return true;
    }

    /**
     * Method to verify if a Location is valid, verify if is not null or is empty
     * If null or empty throws an IllegalArgumentException
     *
     * @param startLocation String to verify
     * @return true if is a valid Location
     */
    private boolean isValidLocation(String startLocation) {
        return startLocation != null && !startLocation.isEmpty() && startLocation.trim().length() != 0;
    }

    /**
     * Method to verify if a StartingTime is valid, verify if is not null
     * If null throws an IllegalArgumentException
     *
     * @param startingTime LocalTime to verify
     * @return true if is a valid LocalTime
     */
    private boolean isValidStartingTime(LocalTime startingTime) {
        if (startingTime == null)
            throw new IllegalArgumentException("Starting Time must be not null");
        return true;
    }

    /**
     * Method to verify if a Date is valid, verify if is not null
     * If null throws an IllegalArgumentException
     *
     * @param date Date to verify
     * @return true if is a valid Date
     */
    private boolean isValidDate(Date date) {
        if (date == null)
            throw new IllegalArgumentException("Date must be not null");
        return true;
    }

    /**
     * Method to verify if a given Date is equals of this.date
     *
     * @param date Date to verify
     * @return true if is same date, false if is different
     */
    public boolean isMyDate(Date date) {
        return this.date.equals(date);
    }

    /**
     * Method to verify if a given UUID is equals of this.UUID
     *
     * @param uuid Date to verify
     * @return true if is same uuid, false if is different
     */
    public boolean isMyUUID(UUID uuid) {
        return this.id.equals(uuid);
    }

    /**
     * Method to Compare Distance
     *
     * @param stage stage to be compared
     * @return the value 0 if anotherDouble is numerically equal to this Double; a value less than 0 if this Double is numerically
     * less than anotherDouble; and a value greater than 0 if this Double is numerically greater than anotherDouble.
     */
    public int compareDistance(Stage stage) {
        StandardStage otherStage = (StandardStage) stage;
        return this.distance.compareTo(otherStage.distance);
    }

    /**
     * Method to create and give a StageDTO of this instance
     *
     * @return StagaDTO with this instance information
     */
    public StageDTO getStageDTO() {
        return new StageDTO(this.date, this.startingTime, this.startLocation, this.finishLocation, this.distance);
    }

    /**
     * Method to Get Distance
     *
     * @return this.distance (Distance attribute of a Stage instance)
     */
    public Double getDistance() {
        return this.distance;
    }
}
