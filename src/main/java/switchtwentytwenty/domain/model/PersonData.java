package switchtwentytwenty.domain.model;

import switchtwentytwenty.domain.dtos.input.PersonDataDTO;

import java.util.Date;

public class PersonData {

    private String personName;
    private Date birthDate;

    public PersonData(PersonDataDTO personDataDTO) {
        this.personName = personDataDTO.getPersonName();
        this.birthDate = personDataDTO.getBirthDatePerson();
    }

}
