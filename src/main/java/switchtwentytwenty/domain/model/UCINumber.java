package switchtwentytwenty.domain.model;

public class UCINumber {

    private String uciNumber;

    public UCINumber(String uciNumber) {
        checkIfIsValidUCINumber(uciNumber);
        this.uciNumber = uciNumber;
    }

    private void checkIfIsValidUCINumber(String uciNumber) {
        if(uciNumber == null || uciNumber.isEmpty()){
            throw new IllegalArgumentException("Invalid UCI Number");
        }
    }

}
