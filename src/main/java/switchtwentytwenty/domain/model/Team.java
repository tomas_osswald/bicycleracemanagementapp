package switchtwentytwenty.domain.model;


import switchtwentytwenty.domain.dtos.input.AthleteDataDTO;
import switchtwentytwenty.domain.dtos.input.CreateTeamDTO;
import switchtwentytwenty.domain.dtos.input.PersonDataDTO;
import switchtwentytwenty.domain.dtos.input.RegisterAthleteDTO;

import java.util.ArrayList;
import java.util.List;

public class Team {

    private String teamName;
    private Representative representative;
    private List<Athlete> athletes = new ArrayList<>();

    public Team(CreateTeamDTO createTeamDTO, Representative representative) {
        String newName = createTeamDTO.getTeamName();
        if (!isNameValid(newName)) {
            throw new IllegalArgumentException("Name is empty or blank");
        }
        this.teamName = newName;
        this.representative = representative;
    }

    public String getTeamName() {
        return teamName;
    }

    public Representative getRepresentative() {
        return representative;
    }

    private boolean isNameValid(String name) {
        boolean isValid = true;
        if (name.isEmpty()) {
            isValid = false;
        }
        return isValid;
    }

    public boolean isThisYourName(String name) {
        boolean isYourName = false;
        if (name != null) {
            if (name.equals(this.teamName)) {
                isYourName = true;
            }
        }
        return isYourName;
    }

    public boolean isYourRepresentative(Representative representative) {
        boolean isYourRepresentative = false;
        if (this.representative.equals(representative)) {
            isYourRepresentative = true;
        }
        return isYourRepresentative;
    }

    public void addAthleteToList(Athlete athlete) {
        athletes.add(athlete);
    }

    public void registerAthlete(RegisterAthleteDTO registerAthleteDTO) {
        PersonDataDTO personDataDTO = registerAthleteDTO.getPersonDataDTO();
        AthleteDataDTO athleteDataDTO = registerAthleteDTO.getAthleteDataDTO();
        Athlete newAthlete = new Cyclist(personDataDTO, athleteDataDTO);
        addAthleteToList(newAthlete);
    }
}
