package switchtwentytwenty.domain.model;

import switchtwentytwenty.domain.dtos.input.AthleteDataDTO;

public class AthleteData {

    private double height;
    private double weight;
    private UCINumber uciNumber;


    public AthleteData(AthleteDataDTO athleteDataDTO) {
        this.height = athleteDataDTO.getHeight();
        this.weight = athleteDataDTO.getWeight();
        this.uciNumber = new UCINumber(athleteDataDTO.getUCINumber());
    }
}
