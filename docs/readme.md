Race
Ao instanciar race o map não é necessário. Só quando estiverem concluídas as inscrições serão atribuidos numeros dorsais e o map iniciado. Race tem uma lista de todas as etapas que lhe pertencem.
Nome será único para cada Race e como tal identificativo. Para funcionamento interno será ainda utilizado um ID gerado automaticamente.

StageList
É criado um StageList para lidar com operações relativas ao cálculo de distância geral e outras operações relativas às stages. Fará sentido existir antes um StageService (conhecido pela Application) que assuma esse papel?
Para actualização automática da Stadings geral da race, esta organização com StageList será mais fácil do que se usarmos um StageService.


Standings
Standings é um HashMap que liga um ciclista a um HashMap de numberPlate e time. Esta lista só será iniciada quando todos os ciclistas estiverem registados, sendo-lhes atribuído um número para a prova e o tempo iniciado a zero.
Utilizamos composição para a standings . Race e Stage terão instancias de standings associadas. Standings deve ser uma classe pois tem comportamentos específicos.

Stage Interface
Terá um id gerado automaticamente que a permite identificar para funcionamento interno. Para funcionamento externo será identificada pela data.
Para os atributos distância, altura e peso não serão especificadas unidades de medida. Estas serão uma regra que será específica a cada implementação da aplicação.
Usamos SimpleDateFormat porque permite controlar as datas com a granularidade pretendida. Stage tem data e hora, mas ambos os valores são representados num só atributo porque não é necessário mais.
Interface porque pode haver vários tipos de Stage, por exemplo: prova standard, contra-relógio, contra-relógio de equipa, circuito.
Interface protege eventuais alterações (padrão Protected Variation e Polymorphism).

Interfaces para Coach e Cyclist:
Há duas interfaces que representam os dois comportamentos existentes numa equipa, os atletas e os treinadores. Como um treinador pode participar na prova como atleta também, existe uma classe que implementa estas duas interfaces.

Por enquanto há dois switchtwentytwenty.domain.services que gerem a lista de equipas e a prova. Eventualmente um terceiro service para gerir as etapas?

DTOs de entrada genéricos? Que possam levar vários dados ou valores nulos, apenas os dados necessários são utilizados?

US01 - RaceService cria a prova? Porque padrão Information Expert e Creator. Dados básicos são nome, data início e data fim.

US02 - StageList cria e adiciona etapas porque padrão Information Expert e Creator e PureFabrication. StageList porque Low Coupling e High Cohesion.

US05 - A race vai dizer ao StageList para devolver as datas em que não existem etapas (entre as datas de início e fim da prova)

US06 - StageList vai calcular a distance total fazendo a soma da distance de cada etapa.

US07 e US08 - StageList vai obter a distance menor/maior das etapas (DTO startTimeAndDate, startLocation, finishLocation, distance)

US10 - TeamService cria e adiciona equipas porque padrão Creator, Information Expert e Pure Fabrication.

US11 - Team cria e adiciona atletas porque padrão Creator e Information Expert. DTO de entrada para proteger futuras alterações de dados.

PERGUNTAS PARA PO:

Dados pertinentes para inscrição do ciclista:

1. Número da federação
2. Nome
3. Data nascimento
4. Peso
5. Altura
6. Nr cartão de saúde
7. Team role
8. Rider type

Geral - É pretendido que os representantes sejam guardados na aplicação? Se sim, que dados seriam necessários?

O número da Federação Internacional de Ciclismo (UCI) deverá ser validado por nós ou simplesmente aceite no acto de registo de um ciclista? Se necessário validar pode fornecer os requisitos deste número, visto não termos encontrado informações na nossa pesquisa?