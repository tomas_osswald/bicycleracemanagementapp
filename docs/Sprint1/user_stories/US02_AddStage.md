# US02 Add Stage

# 1. Requirements

## 1.1. Client Notes

As an administrator, I want to add a stage to the race:

**Demonstration 1** As administrator, I want to...

- 1.1. Add a stage to the race;

**Extracted from communications with the Product Owner**

- >*"The race has a name and is made up of stages, which are done in a given order, with each stage having a date, start time, place of departure, place of arrival and length. There cannot be two stages on the same day."*;
- >*"The race takes place between two dates."*

The interpretation made to this requirement is that the administrator can add stages to the race. For this, him have to provide the date, start time, place of departure and place of arrival. It was also interpreted that there could not be two stages on the same day and must be within the limits of the race (start day to end day).

## 1.2. System Sequence Diagram

```puml
autonumber
title System Sequence Diagram - US02

actor "Administrator" as administrator
participant ": System" as system

activate administrator

administrator -> system : Add stage
activate system
system -> administrator : ask data
deactivate system

administrator -> system : inputs required data (date, start time, place of departure, place of arrival and length)
activate system
alt failure
system -> administrator : Inform Failure

else sucess
system -> administrator : Inform Success

end

deactivate system

deactivate administrator
```

## 1.3. Dependencies from other User Stories

This user story is dependent on the following:

- US01_CreateRace;

# 2. Analysis

From the analysis made to the client's requirements, we concluded that we have to take into account:

- Input needed from administrator:
    - date;
    - start time;
    - place of departure; 
    - place of arrival;
    - length of stage.
    
- Checks to be made:
    - One stage per day;
    - the day of the stage must be within the limits of the race (start day to end day).
     
As the application will only be used for a race, it will not be necessary to identify the race, since there will only be one.

##2.1. Domain Model Diagram

```puml
hide empty members
hide circle
title Domain Model Diagram US135

class Race {
name
}

class Stage {
date
start time
start location
finish location
distance
}

Race "1" --> "1..*" Stage : has
```

# 3. Design

The design carried out based on the analysis is as follows:

There will be a RaceService that contains the Race, the Race class will contain the StageList class, and this will add the Stages (interface).

A StageList is created to handle operations related to the calculation of general distance and other operations related to stages. 

We decided to use a Stage Interface because there may be several types of Stage in the future, for example: standard race, time trial, team time trial, circuit.
The use of the interface protects against possible changes (standard Protected Variation and Polymorphism).
Each stage will have an automatically generated id that allows it to be identified for internal operation. For external operation it will be identified by the date.
For the distance, height and weight attributes, units of measurement will not be specified. These will be a rule that will be specific to each implementation of the application.
We use SimpleDateFormat because it allows you to control dates with the desired granularity. Stage has a date and time, but both values are represented in a single attribute because no more is needed.

It was also decided to use an input DTO to protect future data changes needed to create stages.

At the end, the controller will return:

    - A Result object with **true** state, if a stage was added;
    - A Result object with **false** state and an error message, if if an error occurred and why.

## 3.1 Class Diagram

```puml
title Class Diagram US02

hide empty members

interface Stage {
}

class Application {
}

class RaceService {
}


class Race {
- String name
- int id
- SimpleDateFormat startDate
- SimpleDateFormat endDate
}

class StageList {
}

class StandardStage {
- int id
- SimpleDateFormat startTimeAndDate
- String startLocation
- String finishLocation
- double distance
}


Application *-- RaceService : has

RaceService *-- Race : has


Race *- StageList : has
StageList - Stage : has list >

Stage <|--. StandardStage : implements

```

## 3.2. Functionality Use

```puml
autonumber 1
title addStage
actor "Administrator" as actor
participant ": UI" as UI
participant ": AddStage \n Controller" as controller
participant ": Application" as app
participant "aRaceService : RaceService" as rserv
participant "aRace : Race" as race
participant "aStageList : Stagelist" as stagelist
participant "aStandartStage : StandartStage" as stage

activate actor
actor -> UI : AddStage
activate UI
UI -> actor : ask data
deactivate UI

actor -> UI : inputs required data (date, start time, place of departure, place of arrival and length)
activate UI
UI -> controller : addStage(addStageDTO)
activate controller
controller -> app : getRaceService()
activate app
app -> controller : aRaceService
deactivate app

controller -> rserv : addStage(addStageDTO)
activate rserv
rserv -> race : addStage(addStageDTO)
activate race

race -> stagelist : addStage(addStageDTO)
activate stagelist

stagelist -> stagelist : verifyDate(addStageDTO)

stagelist -> stage** : StandartStage(addStageDTO)

stagelist -> stagelist : addStage(aStandartStage)

stagelist -> race : inform success
deactivate stagelist

race -> rserv : inform success
deactivate race

rserv -> controller : inform sucess
deactivate rserv

alt success
controller -> UI : Result(true)
UI -> actor : Inform Success

else failure (an error has occurred)
controller -> UI : Result(false + error message)
deactivate controller

UI -> actor : Inform Unsuccess and error message
end
deactivate UI
deactivate actor 
```

## 3.3. Applied Patterns

We applied the following principles:

- GRASP:
    -
    - Information expert:
        - The StageList class will be the information expert regarding the Stage Interface, holding all necessary information to perform functions regarding it.
        
    - Creator:
        - The StageList will be the creator of the implementations of Stage Intergace, as it also contains and records instances of it.
     
    - Controller:
        - To deal with the responsibility of receiving input from outside the system (first layer after the UI) we use a case controller.
    
    - Pure Fabrication:
        - In this user story the StageList class was used, which does not represent a business domain concept. It was created to be responsible for all operations regarding the Stages.

    - High cohesion and Low Coupling:
        - The creation of the StageList class provided low Coupling and high Cohesion, keeping one Class as the Information Expert.
          
    - Protected Variation:
        -  With regard to this US, the biggest turning point in the future would be the introduction of different types of Stage, so we implemented the Stage interface for the Stage list to save a list of Stage Interfaces.
                    
- SOLID:
    -
    - Single-responsibility principle:
        - The Single responsibility principle pattern was used in the StandardStage class, in which it only has the responsibility to store information regarding the StandardStage.
        
    - Open-closed principle, Liskov substitution principle and Dependency inversion principle:
        - We have used abstracted interfaces (implement behaviors), where the implementations can be changed and multiple implementations could be created and polymorphically substituted for each other, like in Stage interface.
    
   
        
## 3.4. Tests

### 3.4.1. Controller Tests

**Test 1** : **Add Stage Success** - Stage successfully added
  
    @Test
    void addStageSuccess() {
        CreateRaceController createRaceController = new CreateRaceController(application);
        createRaceController.createRace(createRaceDTO);

        AddStageDTO addStageDTO = new AddStageDTO(validDate, startingTime, starLocation, finishLocation, distance);
        AddStageController addStageController = new AddStageController(application);

        Result<String> expected = Result.success();
        Result<String> result = addStageController.addStage(addStageDTO);

        assertEquals(expected, result);
    }

**Test 2** : **Add Stage Failure** - Race Does not exist

  
    @Test
    void addStageFailureRaceDoesNotExist() {
        AddStageDTO addStageDTO = new AddStageDTO(validDate, startingTime, starLocation, finishLocation, distance);
        AddStageController addStageController = new AddStageController(application);

        Result<String> expected = Result.failure("Race is not created yet!");
        Result<String> result = addStageController.addStage(addStageDTO);

        assertEquals(expected, result);
    }


**Test 3** : **Add Stage Failure** - Stage to be added is Out Of Bounds (Not between Race start and end date)
  
    @Test
    void addStageFailureDateOutOfBoundaries() {
        CreateRaceController createRaceController = new CreateRaceController(application);
        createRaceController.createRace(createRaceDTO);

        Date invalidDate = new Date(2020, 02, 01);

        AddStageDTO addStageDTO = new AddStageDTO(invalidDate, startingTime, starLocation, finishLocation, distance);
        AddStageController addStageController = new AddStageController(application);

        Result<String> expected = Result.failure("Stage date must be inside race boundaries");
        Result<String> result = addStageController.addStage(addStageDTO);

        assertEquals(expected, result);
    }
    
**Test 4** : **Add Stage Failure** - There is already a stage that day
    
    @Test
    void addStageFailureAnotherStageAlreadyExistsInThatDay() {
        CreateRaceController createRaceController = new CreateRaceController(application);
        createRaceController.createRace(createRaceDTO);

        AddStageDTO addStageDTO = new AddStageDTO(validDate, startingTime, starLocation, finishLocation, distance);
        AddStageController addStageController = new AddStageController(application);
        addStageController.addStage(addStageDTO);

        Result<String> expected = Result.failure("Already exists a stage in that day!");

        Result<String> result = addStageController.addStage(addStageDTO);

        assertEquals(expected, result);
    }

### 3.4.2. StandardStage Tests

**Test 5** : **Create StandardStage Success** - A valid instance is made

    @Test
    void aValidStageInstance() {
        StandardStage standardStage = new StandardStage(addStageDTO, uuid);

        assertNotNull(standardStage);
    }

**Test 6** : **Create StandardStage Failure** - Invalid Distance (Zero)

    @Test
    void failureInvalidDistanceZero() {
        Double invalidDistance = 0.00;

        AddStageDTO addStageDTOInvalid = new AddStageDTO(validDate, startingTime, starLocation, finishLocation, invalidDistance);

        assertThrows(IllegalArgumentException.class, () -> new StandardStage(addStageDTOInvalid, uuid));
    }

**Test 7** : **Create StandardStage Failure** - Invalid Distance (Null)

    @Test
    void failureInvalidDistanceNull() {
        Double invalidDistance = 0.00;

        AddStageDTO addStageDTOInvalid = new AddStageDTO(validDate, startingTime, starLocation, finishLocation, invalidDistance);

        assertThrows(IllegalArgumentException.class, () -> new StandardStage(addStageDTOInvalid, uuid));
    }

**Test 8** : **Create StandardStage Failure** - Invalid Start Location (empty)

    @Test
    void failureInvalidStartLocation() {
        String invalidLocation = "";

        AddStageDTO addStageDTOInvalid = new AddStageDTO(validDate, startingTime, invalidLocation, finishLocation, distance);

        assertThrows(IllegalArgumentException.class, () -> new StandardStage(addStageDTOInvalid, uuid));
    }

**Test 9** : **Create StandardStage Failure** - Invalid Finish Location (empty)

    @Test
    void failureInvalidFinishLocation() {
        String invalidLocation = "";

        AddStageDTO addStageDTOInvalid = new AddStageDTO(validDate, startingTime, starLocation, invalidLocation, distance);

        assertThrows(IllegalArgumentException.class, () -> new StandardStage(addStageDTOInvalid, uuid));
    }

**Test 10** : **Create StandardStage Failure** - Invalid Starting Time (null)

    @Test
    void failureInvalidStartingTime() {
        LocalTime invalidTime = null;

        AddStageDTO addStageDTOInvalid = new AddStageDTO(validDate, invalidTime, starLocation, finishLocation, distance);

        assertThrows(IllegalArgumentException.class, () -> new StandardStage(addStageDTOInvalid, uuid));
    }

**Test 11** : **Create StandardStage Failure** - Invalid Date (null)

    @Test
    void failureInvalidDate() {
        Date invalidDate = null;

        AddStageDTO addStageDTOInvalid = new AddStageDTO(invalidDate, startingTime, starLocation, finishLocation, distance);

        assertThrows(IllegalArgumentException.class, () -> new StandardStage(addStageDTOInvalid, uuid));
    }


## 4. Implementation

### 4.1 Method to verify if a Race is already presente in system - RaceService Class

    private void checkRace() {
        if (this.race == null)
            throw new RaceNotPresentException("Race is not created yet!");
    }

### 4.2 Method to verify if a given date is between race start and end date - Race class

    private void verifyDate(Date date) {
        if (date.before(startDate) || date.after(endDate))
            throw new DateOutOfBoundariesException("Stage date must be inside race boundaries");
    }
    
### 4.3 Method to verify if is already a Stage in a given date - StageList class
    
    private void verifyDate(Date date) {
        for (Stage stage : stages) {
            if (stage.isMyDate(date)) {
                throw new StageAlreadyWithThatDateException("Already exists a stage in that day!");
            }
        }
    }    
    
### 4.4 Method to generate and UUID for a stage - StageList class
    
    private UUID generateID() {
        UUID uuid;
        boolean uuidExists;

        do {
            uuidExists = false;
            uuid = UUID.randomUUID();
            for (int i = 0; i < stages.size() || uuidExists; i++) {
                if (stages.get(i).isMyUUID(uuid)) {
                    uuidExists = true;
                }
            }
        } while (uuidExists);

        return uuid;
    }
    
### 4.5 Method to verify if Distance is greater than zero - StandardStage class

    private boolean isValidDistance(Double distance) {
        if (distance <= 0) {
            throw new IllegalArgumentException("Distance must be Greater than Zero");
        }
        return true;
    }
    
### 4.6 Method to verify if a Location is valid, verify if is not null or is empty - StandardStage class

    private boolean isValidLocation(String startLocation) {
        return startLocation != null && !startLocation.isEmpty() && startLocation.trim().length() != 0;
    }

### 4.7 Method to verify if a StartingTime is valid, verify if is not null - StandardStage class

    private boolean isValidStartingTime(LocalTime startingTime) {
        if (startingTime == null)
            throw new IllegalArgumentException("Starting Time must be not null");
        return true;
    }

### 4.8 Method to verify if a Date is valid, verify if is not null - StandardStage class

    private boolean isValidDate(Date date) {
        if (date == null)
            throw new IllegalArgumentException("Date must be not null");
        return true;
    }    
    
# 5. Integration/Demonstration

The development of this US will have an impact on the development of the US05, US06, US07 and US08 that make use of Stage interface implementations, such as StandardStage class.

# 6. Observations

Implementation alternatives: we could have created a service dedicated to the management of the stages that would be contained in the application. We chose this implementation because it is the responsibility of the Race to contain a list of Stages and in the future for automatic updating of the general Stadings of the race, this organization with StageList will be easier than if we use a StageService.