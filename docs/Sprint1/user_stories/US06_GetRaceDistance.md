# US06 Get Race Distance

# 1. Requirements

## 1.1. Client Notes

As an administrator, I want to add a stage to the race:

**Demonstration 1** As administrator, I want to...

- 1.1. know the total length of the race, i.e. the sum of the length of the stages.

The interpretation made to this requirement is that the administrator has access to a feature that lets you know the total distance of the race.

## 1.2. System Sequence Diagram

```puml
autonumber
title System Sequence Diagram - US02

actor "Administrator" as administrator
participant ": System" as system

activate administrator


administrator -> system : Get total race distance
activate system
alt failure
system -> administrator : Inform failure

else sucess
system -> administrator : Inform Total Distance

end

deactivate system

deactivate administrator
```

## 1.3. Dependencies from other User Stories

This user story is dependent on the following:

- US01_CreateRace;
- US02_AddStage.

# 2. Analysis

From the analysis made to the client's requirements, we concluded that we have to take into account:
    
- Checks to be made:
    - If a race already exists;
    - No stages added to race.
     
As the application will only be used for a race, it will not be necessary to identify the race, since there will only be one.

##2.1. Domain Model Diagram

```puml
hide empty members
hide circle
title Domain Model Diagram US135

class Race {
name
}

class Stage {
date
start time
start location
finish location
distance
}

Race "1" --> "1..*" Stage : has
```

# 3. Design

The design carried out based on the analysis is as follows:

There will be a RaceService that contains the Race, the Race class will contain the StageList class, and this will add the Stages (interface).

A StageList is created to handle operations related to the calculation of general distance and other operations related to stages. Then, the iteration of the sum of the total distance will be in charge of the StageList class.

We decided to use a Stage Interface because there may be several types of Stage in the future, for example: standard race, time trial, team time trial, circuit.
The use of the interface protects against possible changes (standard Protected Variation and Polymorphism).
Each stage will have an automatically generated id that allows it to be identified for internal operation. For external operation it will be identified by the date.
For the distance, height and weight attributes, units of measurement will not be specified. These will be a rule that will be specific to each implementation of the application.
We use SimpleDateFormat because it allows you to control dates with the desired granularity. Stage has a date and time, but both values are represented in a single attribute because no more is needed.

At the end, the controller will return:

   - A Result object with **true** state, with total distance;
        - If there is a race but with no stages, the total distance will be Zero (0);
   - A Result object with **false** state and an error message, if if an error occurred and why (There is no race present).

## 3.1 Class Diagram

```puml
title Class Diagram US02

hide empty members

interface Stage {
}

class Application {
}

class RaceService {
}


class Race {
- String name
- int id
- SimpleDateFormat startDate
- SimpleDateFormat endDate
}

class StageList {
}

class StandardStage {
- int id
- SimpleDateFormat startTimeAndDate
- String startLocation
- String finishLocation
- double distance
}


Application *-- RaceService : has

RaceService *-- Race : has


Race *- StageList : has
StageList - Stage : has list >

Stage <|--. StandardStage : implements

```

## 3.2. Functionality Use

```puml
autonumber 1
title getRaceDistance.1
actor "Administrator" as actor
participant ": UI" as UI
participant ": GetStageByDistanceController \n Controller" as controller
participant ": Application" as app
participant "aRaceService : RaceService" as rserv
participant "aRace : Race" as race
participant "aStageList : Stagelist" as stagelist

activate actor

actor -> UI : Get Total Race Distance
activate UI
UI -> controller : getRaceDistance()
activate controller
controller -> app : getRaceService()
activate app
app -> controller : aRaceService
deactivate app

controller -> rserv : getRaceDistance()
activate rserv
rserv -> race : getRaceDistance()
activate race

race -> stagelist : getRaceDistance()
activate stagelist


ref over stagelist 
getRaceDistance.2
end 

stagelist -> race : totalDistance
deactivate stagelist

race -> rserv : totalDistance
deactivate race

rserv -> controller : totalDistance
deactivate rserv

alt success
controller -> UI : Result(true + totalDistance)
UI -> actor : Inform totalDistance

else failure (an error has occurred)
controller -> UI : Result(false + error message)
deactivate controller

UI -> actor : Inform Unsuccess and error message
end
deactivate UI
deactivate actor 
```

```puml
autonumber 1
title getRaceDistance.2

participant "aStageList : Stagelist" as stagelist
participant "atage : Stage" as stage

--> stagelist : getRaceDistance()
activate stagelist

stagelist --> stagelist : distance = 0.00
loop for each stage in stageList

stagelist --> stage : stage.getDistance()
activate stage
stage -> stagelist : stageDistance
deactivate stage

stagelist --> stagelist : distance += stageDistance

end

<-- stagelist : totalDistance

deactivate stagelist
```
 


## 3.3. Applied Patterns

We applied the following principles:

- GRASP:
    -
    - Information expert:
        - The StageList class will be the information expert regarding the Stage Interface, holding all necessary information to perform functions regarding it.
        - Stage Interface implementations are the information expert of their attributes, using tell don't ask when comparing two Stage instances, in which it is the implementation itself that returns the result of the comparison.
        
    - Creator:
        - The StageList will be the creator of the implementations of Stage Intergace, as it also contains and records instances of it.
     
    - Controller:
        - To deal with the responsibility of receiving input from outside the system (first layer after the UI) we use a case controller.
    
    - Pure Fabrication:
        - In this user story the StageList class was used, which does not represent a business domain concept. It was created to be responsible for all operations regarding the Stages.

    - High cohesion and Low Coupling:
        - The creation of the StageList class provided low Coupling and high Cohesion, keeping one Class as the Information Expert.
          
    - Protected Variation:
        -  With regard to this US, the biggest turning point in the future would be the introduction of different types of Stage, so we implemented the Stage interface for the Stage list to save a list of Stage Interfaces.
                    
- SOLID:
    -
    - Single-responsibility principle:
        - The Single responsibility principle pattern was used in the StandardStage class, in which it only has the responsibility to store information regarding the StandardStage.
        
    - Open-closed principle, Liskov substitution principle and Dependency inversion principle:
        - We have used abstracted interfaces (implement behaviors), where the implementations can be changed and multiple implementations could be created and polymorphically substituted for each other, like in Stage interface.
    
        
## 3.4. Tests

### 3.4.1. Controller Tests

**Test 1** : **Get Race Distance Success** - One Stage

    @Test
    void getRaceDistanceSuccessMoreThanZeroOneStage() {
        createRaceController.createRace(createRaceDTO);
        addStageController.addStage(addStageDTOOne); // 40.00

        Result<Double> expected = Result.success(40.00);

        Result<Double> result = getStageByDistanceController.getRaceDistance();

        assertEquals(expected, result);
    }

**Test 2** : **Get Race Distance Success** - Two Stage

    @Test
    void getRaceDistanceSuccessMoreThanZeroTwoStages() {
        createRaceController.createRace(createRaceDTO);
        addStageController.addStage(addStageDTOOne); // 40.00
        addStageController.addStage(addStageDTOTwo); // 60.00

        Result<Double> expected = Result.success(100.00);

        Result<Double> result = getStageByDistanceController.getRaceDistance();

        assertEquals(expected, result);
    }

**Test 3** : **Get Race Distance Success** - No Stages

    @Test
    void getRaceDistanceSuccessNoStages() {
        createRaceController.createRace(createRaceDTO);

        Result<Double> expected = Result.success(0.00);

        Result<Double> result = getStageByDistanceController.getRaceDistance();

        assertEquals(expected, result);
    }

**Test 4** : **Get Race Distance Unsuccess** - No Race

    @Test
    void getRaceDistanceUnsuccessNoRace() {
        Result<Double> expected = Result.failure("Race is not created yet!");

        Result<Double> result = getStageByDistanceController.getRaceDistance();

        assertEquals(expected, result);
    }

## 4. Implementation

### 4.1 Method to verify if a Race is already presente in system - RaceService Class

    private void checkRace() {
        if (this.race == null)
            throw new RaceNotPresentException("Race is not created yet!");
    }
    
### 4.2 Method to get Total Race Distance - StageList Class

    public Double getRaceDistance() {
        Double distance = 0.00;

        for (Stage stage : stages) {
            distance += stage.getDistance();
        }
        return distance;
    }

# 5. Integration/Demonstration

The development of this US will not have an impact on the development of the others Users Stories of this Sprint.

# 6. Observations

Implementation alternatives: we could have created a service dedicated to the management of the stages that would be contained in the application. We chose this implementation because it is the responsibility of the Race to contain a list of Stages and in the future for automatic updating of the general Stadings of the race, this organization with StageList will be easier than if we use a StageService.