# US05 Get Off Days

# 1. Requirements

## 1.1. Client Notes

As an administrator, I want to know the off days of a race, i.e. days without stages:

**Demonstration 1** As an administrator, I want to...

- 1.1. obtain a list of days without stages;

**Extracted from communications with the Product Owner**

- >*"A prova realiza-se entre duas datas, que são conhecidas antes da sua criação. "*
- >*"Não pode haver duas etapas no mesmo dia."*;

The interpretation made to this requirement is that the administrator wants to know the days off. There is no data input necessary for this function.

## 1.2. System Sequence Diagram

```puml
autonumber
title System Sequence Diagram - US01

actor "Administrator" as administrator
participant ": System" as system

activate administrator

administrator -> system : Get days off
activate system

alt failure
system -> administrator : Inform failure

else succes
system -> administrator : List of days off
deactivate system

end

deactivate administrator
```

## 1.3. Dependencies from other User Stories

This user story is dependent on the following user stories:
- US01_CreateRace
- US02_AddStage

# 2. Analysis

From the analysis made to the client's requirements, we concluded that we have to take into account:

- No input required from the user. The system holds all information necessary to obtain days of the race with no stage.

- The race has a start date and end date. For every date between them, a check will be made to see if a stage occurs on the same day. If there is no stage on that day, it is a day off and will be added to a list.

- Expected outcomes are:
    - a list with one or more dates
    - an empty list when there are no days off
    - an error message if no race has been created yet.

##2.1. Domain Model Diagram

```puml
hide empty members
hide circle
title Domain Model Diagram US135

class Race {
name
start date
end date
}

class Stage {
date
start time
start location
finish location
distance
}

Race "1" --> "1..*" Stage : has
```


# 3. Design


## 3.1 Class Diagram

```puml
title Class Diagram US05
hide empty members


class Application {
}

class RaceService {
}

class Race {
- String name
- int id
- SimpleDateFormat startDate
- SimpleDateFormat endDate
}

class StageList {
}

interface Stage {
}

class StandardStage {
- int id
- SimpleDateFormat startTimeAndDate
- String startLocation
- String finishLocation
- double distance
}

Application *-- RaceService : has >

RaceService *-- Race : has >

Race *- StageList : has >
StageList *- Stage : has list >
Stage .-|> StandardStage : implements
```

## 3.2. Functionality Use

```puml
autonumber
title System Sequence Diagram - US01

actor "Administrator" as administrator
participant "UI" as UI
participant ": GetOffDaysController" as controller
participant ": Application" as application
participant ": RaceService" as raceService
participant "aRace : Race" as race
participant ": StageList" as stageList
participant "aStage : Stage" as stage

activate administrator

administrator -> UI : Get days off

activate UI

UI -> controller : getDaysOff()
activate controller

controller -> application : getRaceService()
activate application
application --> controller : raceService
deactivate application

controller -> raceService : getDaysOff()
activate raceService

alt exception caught : no race

raceService --> controller : exception : no race
controller --> UI : Result: false, errorMessage
UI -> administrator : Inform Failure

else no exception

raceService -> race : getDaysOff()
activate race

race -> stageList : getDaysOff(startDate, endDate)
activate stageList

loop for every day between start and end date
stageList --> stageList : isDayOff = true
loop for every stage in stageList
stageList -> stage : hasSameDate(date)
activate stage
alt same date
stage --> stageList : true
stageList --> stageList : isDayOff = false
else different date
stage --> stageList : false
deactivate stage

end
end
alt isDayOff = true
stageList -> stageList : add to list
end
end

stageList --> race : ListOfDaysOff
deactivate stageList

race --> raceService : ListOfDaysOff
deactivate race

raceService --> controller : ListOfDaysOff
deactivate raceService

controller --> UI : Result: true
deactivate controller

UI -> administrator : Inform Success
deactivate UI

end

deactivate administrator
```


## 3.3. Applied Patterns

We applied the following principles:

- GRASP:
    - Information expert:
        - The StageList class will be the information expert regarding the Stage objects, holding all necessary information to perform functions regarding it.

    - Controller:
        - To deal with the responsibility of receiving input from outside the system (first layer after the UI) we use a case controller.

    - Pure Fabrication:
        - In this user story the StageList class was used, which does not represent a business domain concept. It was created to be responsible for all operations regarding the Stages.

    - High cohesion and Low Coupling:
        - The creation of the StageList class provided low Coupling and high Cohesion.

## 3.4. Tests


## 4. Implementation



# 5. Integration/Demonstration


# 6. Observations

