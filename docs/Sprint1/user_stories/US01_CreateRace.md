# US01 Create Race

# 1. Requirements

## 1.1. Client Notes

As an administrator, I want to create a race, inserting base data:

**Demonstration 1** As an administrator, I want to...

- 1.1. create a race;

**Extracted from communications with the Product Owner**

- >*"A prova realiza-se entre duas datas, que são conhecidas antes da sua criação. "*
- >*"A prova tem um nome e é composta por etapas(...)."*;

The interpretation made to this requirement is that the administrator will create the race this application will manage. For this, he will have to provide the name, start and end date.

## 1.2. System Sequence Diagram

```puml
autonumber
title System Sequence Diagram - US01

actor "Administrator" as administrator
participant ": System" as system

activate administrator

administrator -> system : Create Race
activate system
system -> administrator : ask data
deactivate system

administrator -> system : input required data (name, start date, end date)
activate system
alt failure
system -> administrator : Inform Failure

else sucess
system -> administrator : Inform Success
deactivate system

end

deactivate administrator
```

## 1.3. Dependencies from other User Stories

This user story is not dependent on any other user story.

# 2. Analysis

From the analysis made to the client's requirements, we concluded that we have to take into account:

- Input needed from administrator:
    - name
    - start date;
    - end date;

- Checks to be made:
    - start date is before or on the same day as the end date (in case of a single stage race);
    - name must not be empty or blank.

Other relevant data the Race will have is a list of stages and the general standings. However, this data is not required for its creation.

##2.1. Domain Model Diagram

```puml
hide empty members
hide circle
title Domain Model Diagram US135

class Race {
name
}

class Stage {
date
start time
start location
finish location
distance
}

Race "1" --> "1..*" Stage : has
```


# 3. Design


## 3.1 Class Diagram

```puml
title Class Diagram US01
hide empty members


class Application {
}

class RaceService {
}

class Race {
- String name
- int id
- SimpleDateFormat startDate
- SimpleDateFormat endDate
}

class StageList {
}

Application *-- RaceService : has >

RaceService *-- Race : has >

Race *- StageList : has >

```

## 3.2. Functionality Use

```puml
autonumber
title System Sequence Diagram - US01

actor "Administrator" as administrator
participant "UI" as UI
participant ": CreateRaceController" as controller
participant ": Application" as application
participant ": RaceService" as raceService
participant "aRace : Race" as race

activate administrator

administrator -> UI : Create Race
activate UI
UI -> administrator : ask data
deactivate UI

administrator -> UI : input required data (name, start date, end date)
activate UI

UI -> controller : createRace\n(createRaceDTO)
activate controller

controller -> application : getRaceService()
activate application
application --> controller : raceService
deactivate application

controller -> raceService : createRace(createRaceDTO)
activate raceService

raceService -> race ** : creates(createRaceDTO)

activate race
race -> race : checkDates()
race -> race : checkName()

alt exception caught
race --> raceService : exception
deactivate race


raceService --> controller : false
controller --> UI : Result: false, errorMessage
UI -> administrator : Inform Failure


else no exception
raceService -> raceService : addRace()

raceService --> controller : true
deactivate raceService

controller --> UI : Result: true
deactivate controller

UI -> administrator : Inform Success
deactivate UI

end

deactivate administrator
```


## 3.3. Applied Patterns

We applied the following principles:

- GRASP:
    - Information expert:
        - The RaceService class will be the information expert regarding the Race class, holding all necessary information to perform functions regarding it.

    - Creator:
        - RaceService will be the creator of the Race class, as it also contains and records instances of it.

    - Controller:
        - To deal with the responsibility of receiving input from outside the system (first layer after the UI) we use a case controller.

    - Pure Fabrication:
        - In this user story the RaceService class was used, which does not represent a business domain concept. It was created to be responsible for all operations regarding the Race.

    - High cohesion and Low Coupling:
        - The creation of the RaceService class provided low Coupling and high Cohesion, keeping one Class as the Information Expert.

## 3.4. Tests


## 4. Implementation



# 5. Integration/Demonstration


# 6. Observations

