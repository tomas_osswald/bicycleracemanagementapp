# US10 As a Representative, I want to register a team and later add team riders.

# 1. Requirements

## 1.1. Client Notes

As a Representative, I want to register a team and later add team riders:

**Demonstration 1** As Representative, I want to...

- 1.1. register a team and later add team riders;


The interpretation made to this requirement is that the Representative can register a team. For this, he has to provide the name of team and other detais.

## 1.2. System Sequence Diagram

```` puml

    autonumber
    title Register Team SSD
    actor "Team Representative" as teamAdmin
    participant "System" as system

    activate teamAdmin
    teamAdmin -> system: register Team
    activate system
   system --> teamAdmin: ask for team data
   teamAdmin->system: send required team data
    system --> teamAdmin: inform success
    deactivate system
    deactivate teamAdmin
@endpuml
````

## 1.3. Dependencies from other User Stories

This user story has no dependencies.

# 2. Analysis

From the analysis made to the client's requirements, we concluded that we have to take into account:

- Input needed from representative :
    - name of team;
    - country of origin;
   
- Checks to be made:
    - name must be unique;
    - country of origin must be existent.

As the application will only be used for a race, it will not be necessary to identify the race, since there will only be one.

##2.1. Domain Model Diagram

```puml
hide empty members
hide circle
title Domain Model Diagram US10

class Representative {
name
}

class Team {
name
country
}

Representative "1" --> "1..*" Team : has
```

# 3. Design

## 3.1 Class Diagram

```puml
@startuml

hide empty members

class Application {
}

class TeamService {
}

class RepresentativeService {
}

interface StaffMember {
}

interface Athlete {
}

class Team {
name
}

class Representative {
}

class Cyclist {
number plate
}

class CyclistCoach {
number plate
}

class Coach {
}

class PersonData {
- id
- name
- date of birth
}

class Email {
String email
}

class CyclistData {
double weight
double height
}

Application *-- TeamService : has
Application *-- RepresentativeService : has
RepresentativeService --o TeamService : has <

TeamService *-- Team : has list >
'TeamService -- Representative : has list >
Representative --o Team : has <
RepresentativeService *-- Representative : has list >



Team *-left- StaffMember : has
Team *-- Athlete : has list >
Cyclist .up-|> Athlete : implements
CyclistCoach .up-|> Athlete : implements
CyclistCoach .up-|> StaffMember : implements
Coach .up-|> StaffMember : implements

CyclistData *-right- UCINumber

Cyclist *-- PersonData
CyclistCoach *-- PersonData
Coach *-- PersonData
Representative *-- PersonData
Representative *-- Email
Cyclist *-- CyclistData
CyclistCoach *-- CyclistData

@enduml
```

## 3.2. Functionality Use

### Sequence diagram

``` puml
autonumber 1
title register Team
actor "Representative" as actor
participant ": UI" as UI
participant ": registerTeam\n Controller" as controller
participant ": Application" as app
participant "aTeamService\n : teamService" as serv
participant "aRepresentativeService\n : RepresentativeService" as repserv
participant "newTeam : Team" as newTeam

activate actor
actor -> UI : create a Team
activate UI
UI -> actor : ask: teamName, teamCountry

actor -> UI : inputs required data

UI -> controller : createTeam(createTeamDTO)

activate controller
controller -> app : getTeamService()
activate app
return aTeamService


controller -> serv : createTeam(createTeamDTO)
activate serv
serv --> serv : representativeEmail = \ncreateTeamDTO.getEmail()

serv --> repserv : getRepresentative\n(representativeEmail)
activate repserv

repserv --> repserv : checkEmailOfAnyRepresentative\n(representativeEmail)
activate repserv
alt throw exception no such representative
repserv --> controller : throw exception
deactivate repserv
controller --> UI : Result<T> failure
UI --> actor : inform failure
else representative found
return aRepresentative
end

serv --> serv : checkRepresentativeOfAnyTeam\n(aRepresentative)
activate serv
alt throw exception already represents a team
serv --> controller : throw exception
deactivate serv
controller --> UI : Result<T> failure
UI --> actor : inform failure
end

serv --> serv : teamName = \ncreateTeamDTO.getName()


serv --> serv : checkIfNameOfAnyTeam\n(teamName)
activate serv
alt throw exception name already in use
serv --> controller : throw exception
deactivate serv
controller --> UI : Result<T> failure
UI --> actor : inform failure
end

serv --> newTeam **: createTeam(createTeamDTO, aRepresentative)

serv --> serv : addTeamToList(newTeam)

deactivate serv

return Result<T> success

return inform success
```

for (int i = 0; i < list.size() && isRep = false; i++)
    if(list.get(i).isThisYourID(representativeID)) {
        aRepresentative = list.get(i);
        isRep = true;
    }

## 3.3. Applied Patterns

We applied the following principles:

TeamService cria e adiciona equipas porque padrão Creator, Information Expert e Pure Fabrication.

- GRASP:
    - Information expert:
        - This pattern was used in classes that implemented the Account interface, like in this case CashAccount class, for returning a DTO with the account id and description without removing information outside the class;

    - Controller:
        - To deal with the responsibility of receiving input from outside the system (first layer after the UI) we use a case controller.

    - Pure Fabrication:
        - In this user story the Application and AccountService class was used, which does not represent a business domain concept. It was created to be responsible for all operations regarding Account type Classes.

    - High cohesion and Low Coupling:
        - The creation of the AccountService class provided low Coupling and high Cohesion, keeping one Class as the Information Expert.

    - Protected Variation:
        - An Account interface was used in which the polymorphism was used to be implemented in several classes, each representative of a type of Account.

- SOLID:
    - Single-responsibility principle:
        - this pattern was used in the AccountService class, in which it the only responsibility is manage account operations.

## 3.4. Tests

**Tests :** Controller:

#### Success cases:

**Test X** : **Unknown** blabla **false-true**
    
    @Test

    }
    
#### Failure cases:

**Test X** : **Unknown** blabla **false-true**

    @Test
    
    }

**Test X** : **Unknown** blabla **false-true**

    @Test

    }

**Test X** : **Unknown** blabla **false-true**

    @Test

    }


## 4. Implementation



# 5. Integration/Demonstration


# 6. Observations



