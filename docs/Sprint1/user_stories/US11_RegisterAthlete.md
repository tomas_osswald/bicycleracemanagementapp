# US11 As a Representative, I want to register athletes for my team.

# 1. Requirements

## 1.1. Client Notes

As a Representative, I want to register athletes for my team:

**Demonstration 1** As Representative, I want to...

- 1.1. register athletes for my team;

The interpretation made to this requirement is that the Representative can register a team. For this, he has to provide the name of team and other detais.

## 1.2. System Sequence Diagram

```` puml

    autonumber
    title Register Athletes SSD
    actor "Team Representative" as teamAdmin
    participant "System" as system

    activate teamAdmin
    teamAdmin -> system: register Athlete
    activate system
   system --> teamAdmin: ask for team data
   teamAdmin->system: send required team data
    system --> teamAdmin: inform success
    deactivate system
    deactivate teamAdmin
@endpuml
````

## 1.3. Dependencies from other User Stories

This user story has no dependencies.

# 2. Analysis

From the analysis made to the client's requirements, we concluded that we have to take into account:

- Input needed from representative :
    - name of team;
    - registration number UCI

- Checks to be made:
    - name must be unique;
    - athlete must have registration number UCI

As the application will only be used for a race, it will not be necessary to identify the race, since there will only be one.

##2.1. Domain Model Diagram

```puml
hide empty members
hide circle
title Domain Model Diagram US10

class Representative {
name
}

class Team {
name
country
}

interface Athlete {
athleteName
birthDate
height
double weight
registrationNumberUCI
}

Representative "1" --> "1..*" Team : has
Team "1" --> "0..*" Athlete : has
```

# 3. Design

## 3.1 Class Diagram

```puml
hide empty members

class createTeamController {
+ createTeam()
}

class createTeamDTO {
+ String teamName
+ String teamCountry
}

class Application {
}

class TeamService {
}


interface Athlete {
+ String athleteName
+ String birthDate
+ double height
+ double weight
+ String registrationNumberUCI
}

class Team {
name
}

class Representative {
name
id
}

Application --* createTeamController  : has
Application *-- TeamService  : has
createTeamController --> TeamService : calls
createTeamController <-- createTeamDTO : accepts
TeamService --> Team : has list <Team>
TeamService --> Representative : has list <Representative>
Representative *- Team

Team -- Athlete : has list <Athlete>
```

## 3.2. Functionality Use

### Sequence diagram

``` puml
autonumber 1
title register Athlete
actor "Representative" as actor
participant ": UI" as UI
participant ": RegisterAthlete\n Controller" as controller
participant ": Application" as app
participant "aTeamService\n : teamService" as serv
participant "aRepresentativeService\n : RepresentativeService" as repserv
participant "aTeam : Team" as aTeam


activate actor
actor -> UI : register an athlete
activate UI
UI -> actor : ask: required data

actor -> UI : inputs required data

UI -> controller : registerAthlete\n(registerAthleteDTO)

activate controller
controller -> app : getTeamService()
activate app
return aTeamService


controller -> serv : registerAthlete(registerAthleteDTO)
activate serv
serv --> serv : representativeEmail = \nregisterAthleteDTO.getEmail()

serv --> repserv : getRepresentative\n(representativeEmail)
activate repserv

repserv --> repserv : isEmailOfAnyRepresentative\n(representativeEmail)
activate repserv
alt throw exception no such representative
repserv --> controller : throw exception
deactivate repserv
controller --> UI : Result<T> failure
UI --> actor : inform failure
else representative found
return aRepresentative
end

serv --> serv : getTeamByRepresentative\n(aRepresentative)
activate serv
alt throw exception no team represented by this representative
serv --> controller : throw exception
controller --> UI : Result<T> failure
UI --> actor : inform failure
else found team represented by this representative
return aTeam
end

serv --> aTeam : registerAthlete(registerAthleteDTO)
activate aTeam

ref over aTeam
create Athlete
end
autonumber 29
aTeam --> aTeam : addAthleteToList\n(newAthlete)
deactivate aTeam
deactivate serv
return Result<T> success

return inform success
```

```puml
autonumber 20
title create Athlete
participant "aTeam : Team" as aTeam
participant "newAthlete\n : Athlete" as athlete
participant "newPersonData\n : PersonData" as personData
participant "newAthleteData\n : AthleteData" as athleteData
participant "newUCINumber\n : UCINumber" as uciNumber

--> aTeam : registerAthlete\n(registerAthleteDTO)
activate aTeam

aTeam --> aTeam : createAthleteDTO = \nregisterAthleteDTO.getCreateAthleteDTO()
aTeam --> athlete ** : create(registerAthleteDTO)

activate athlete
athlete --> athlete : personDataDTO = \ncreateAthleteDTO.getPersonDataDTO()
athlete --> personData ** : create(personDataDTO)
athlete --> athlete : athleteDataDTO = \ncreateAthleteDTO.getAthleteDataDTO()
athlete --> athleteData ** : create(athleteDataDTO)
activate athleteData
athleteData --> athleteData : uciNumber = \nathleteDataDTO.getUCINumber()
athleteData --> uciNumber ** : create(uciNumber)
deactivate athleteData
deactivate athlete

aTeam --> aTeam : addAthleteToList(newAthlete)
deactivate aTeam
```

## 3.3. Applied Patterns

We applied the following principles:

TeamService cria e adiciona equipas porque padrão Creator, Information Expert e Pure Fabrication.

- GRASP:
  - Information expert:
    - This pattern was used in classes that implemented the Account interface, like in this case CashAccount class, for returning a DTO with the account id and description without removing information outside the class;

  - Controller:
    - To deal with the responsibility of receiving input from outside the system (first layer after the UI) we use a case controller.

  - Pure Fabrication:
    - In this user story the Application and AccountService class was used, which does not represent a business domain concept. It was created to be responsible for all operations regarding Account type Classes.

  - High cohesion and Low Coupling:
    - The creation of the AccountService class provided low Coupling and high Cohesion, keeping one Class as the Information Expert.

  - Protected Variation:
    - An Account interface was used in which the polymorphism was used to be implemented in several classes, each representative of a type of Account.

- SOLID:
  - Single-responsibility principle:
    - this pattern was used in the AccountService class, in which it the only responsibility is manage account operations.

## 3.4. Tests

**Tests :** Controller:

#### Success cases:

**Test X** : **Unknown** blabla **false-true**

    @Test

    }

#### Failure cases:

**Test X** : **Unknown** blabla **false-true**

    @Test
    
    }

**Test X** : **Unknown** blabla **false-true**

    @Test

    }

**Test X** : **Unknown** blabla **false-true**

    @Test

    }


## 4. Implementation



# 5. Integration/Demonstration


# 6. Observations