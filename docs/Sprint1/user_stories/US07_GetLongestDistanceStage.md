# US05 Get Longest Distance Stage

# 1. Requirements

## 1.1. Client Notes

As an administrator, I want to know the Stage with the longest distance:

**Demonstration 1** As an administrator, I want to...

- 1.1. obtain the stage with the longest distance;

**Extracted from communications with the Product Owner**

- >*"(...) sendo que cada etapa tem uma data, hora de início, local de partida, local de chegada e comprimento."*

The interpretation made to this requirement is that the administrator wants to know the stage with the longest distance. There is no data input necessary for this function.

## 1.2. System Sequence Diagram

```puml
autonumber
title System Sequence Diagram - US07

actor "Administrator" as administrator
participant ": System" as system

activate administrator

administrator -> system : Get Stage with longest distance
activate system

alt failure
system -> administrator : Inform failure

else succes
system -> administrator : List of stages with longest distance
deactivate system

end

deactivate administrator
```

## 1.3. Dependencies from other User Stories

This user story is dependent on the following user stories:
- US01_CreateRace
- US02_AddStage

# 2. Analysis

From the analysis made to the client's requirements, we concluded that we have to take into account:

- No input required from the user. The system holds all information necessary to obtain a list of the longest Stages.

- Every Stage has a distance attribute. The StageList can compare distances between Stage instances and create a list with the longest ones. A list will be necessary as there can be two stages with the same distance.

- Expected outcomes are:
    - a list with one stage.
    - a list with many stages if two or more stages have equal longest distances.
    - an error message if no race has been created yet or if no stages have been added to the race.

##2.1. Domain Model Diagram

```puml
hide empty members
hide circle
title Domain Model Diagram US07

class Race {
name
start date
end date
}

class Stage {
date
start time
start location
finish location
distance
}

Race "1" --> "1..*" Stage : has
```


# 3. Design


## 3.1 Class Diagram

```puml
title Class Diagram US07
hide empty members


class Application {
}

class RaceService {
}

class Race {
- String name
- int id
- SimpleDateFormat startDate
- SimpleDateFormat endDate
}

class StageList {
}

interface Stage {
}

class StandardStage {
- int id
- SimpleDateFormat startTimeAndDate
- String startLocation
- String finishLocation
- double distance
}

Application *-- RaceService : has >

RaceService *-- Race : has >

Race *- StageList : has >
StageList *- Stage : has list >
Stage .-|> StandardStage : implements
```

## 3.2. Functionality Use

```puml
autonumber
title System Sequence Diagram - US07

actor "Administrator" as administrator
participant "UI" as UI
participant ": GetLongestDistance\nStageController" as controller
participant ": Application" as application
participant ": RaceService" as raceService
participant "aRace : Race" as race
participant ": StageList" as stageList
participant "aStage : Stage" as stage

activate administrator

administrator -> UI : Get stage with \nlongest distance

activate UI

UI -> controller : getLongestDistanceStage()
activate controller

controller -> application : getRaceService()
activate application
application --> controller : raceService
deactivate application

controller -> raceService : getLongestDistanceStage()
activate raceService

alt exception caught : no race

raceService --> controller : exception : no race
controller --> UI : Result: false, errorMessage
UI -> administrator : Inform Failure

end

raceService -> race : getLongestDistanceStage()
activate race

race -> stageList : getLongestDistanceStage()
activate stageList

alt exception caught : no race
stageList --> controller : exception : no stages
controller --> UI : Result: false, errorMessage
UI -> administrator : Inform Failure
end

stageList --> stageList : longest = firstStage
stageList --> stageList : List<Stage> longestList
stageList --> stageList : longestList.add(longest)
loop for every other stage in stageList
stageList -> stage : compareDistance(longest)
activate stage

alt longer
stage --> stageList : 1
stageList --> stageList : longest = aStage
stageList --> stageList : longestList.clear
stageList --> stageList : longestList.add(longest)

else equal
stage --> stageList : 0
stageList --> stageList : longestList.add(aStage)

else shorter
stage --> stageList : -1
deactivate stage

end
end


stageList --> stageList : stageDTOList = \ncreateStageDTOList\n(longestList)


return StageDTOList

return StageDTOList

return StageDTOList

return Result: true, StageDTOList

return Display List

deactivate administrator
```


## 3.3. Applied Patterns

We applied the following principles:

- GRASP:
    - Information expert:
        - The StageList class will be the information expert regarding the Stage objects, holding all necessary information to perform these functions. The comparison between stages is made by the Stage instances themselves.

    - Controller:
        - To deal with the responsibility of receiving input from outside the system (first layer after the UI) we use a case controller.

    - Pure Fabrication:
        - In this user story the StageList class was used, which does not represent a business domain concept. It was created to be responsible for all operations regarding the Stages.

    - High cohesion and Low Coupling:
        - The creation of the StageList class provided low Coupling and high Cohesion.

## 3.4. Tests


## 4. Implementation



# 5. Integration/Demonstration


# 6. Observations

