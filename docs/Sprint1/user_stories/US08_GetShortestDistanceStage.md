# US08 Get Shortest Distance Stage

# 1. Requirements

## 1.1. Client Notes

As an administrator, I want to know the Stage with the shortest distance:

**Demonstration 1** As an administrator, I want to...

- 1.1. obtain the stage with the shortest distance;

**Extracted from communications with the Product Owner**

- >*"(...) sendo que cada etapa tem uma data, hora de início, local de partida, local de chegada e comprimento."*

The interpretation made to this requirement is that the administrator wants to know the stage with the shortest distance. There is no data input necessary for this function.

## 1.2. System Sequence Diagram

```puml
autonumber
title System Sequence Diagram - US07

actor "Administrator" as administrator
participant ": System" as system

activate administrator

administrator -> system : Get Stage with shortest distance
activate system

alt failure
system -> administrator : Inform failure

else succes
system -> administrator : List of stages with shortest distance
deactivate system

end

deactivate administrator
```

## 1.3. Dependencies from other User Stories

This user story is dependent on the following user stories:
- US01_CreateRace
- US02_AddStage

# 2. Analysis

From the analysis made to the client's requirements, we concluded that we have to take into account:

- No input required from the user. The system holds all information necessary to obtain a list of the shortest Stages.

- Every Stage has a distance attribute. The StageList can compare distances between Stage instances and create a list with the shortest ones. A list will be necessary as there can be two stages with the same distance.

- Expected outcomes are:
    - a list with one stage.
    - a list with many stages if two or more stages have equal shortest distances.
    - an error message if no race has been created yet or if no stages have been added to the race.

##2.1. Domain Model Diagram

```puml
hide empty members
hide circle
title Domain Model Diagram US07

class Race {
name
start date
end date
}

class Stage {
date
start time
start location
finish location
distance
}

Race "1" --> "1..*" Stage : has
```


# 3. Design


## 3.1 Class Diagram

```puml
title Class Diagram US07
hide empty members

class Application {
}

class RaceService {
}

class Race {
- String name
- int id
- SimpleDateFormat startDate
- SimpleDateFormat endDate
}

class StageList {
}

class StandardStage {
- int id
- SimpleDateFormat startTimeAndDate
- String startLocation
- String finishLocation
- double distance
}

interface Stage{
}

Application *-- RaceService : has

RaceService *-- Race : has

Race *- StageList : has

StageList - Stage : has list

Stage <|-. StandardStage : implements
```

## 3.2. Functionality Use

```puml
autonumber
title System Sequence Diagram - US07

actor "Administrator" as administrator
participant "UI" as UI
participant ": GetStageByDistance\nController" as controller
participant ": Application" as application
participant ": RaceService" as raceService
participant "aRace : Race" as race
participant ": StageList" as stageList
participant "aStage : Stage" as stage

activate administrator

administrator -> UI : Get stage with \nshortest distance

activate UI

UI -> controller : getshortestDistanceStage()
activate controller

controller -> application : getRaceService()
activate application
application --> controller : raceService
deactivate application

controller -> raceService : getshortestDistanceStage()
activate raceService

alt exception caught : no race

raceService --> controller : exception : no race
controller --> UI : Result: false, errorMessage
UI -> administrator : Inform Failure

else no exception

raceService -> race : getshortestDistanceStage()
activate race

race -> stageList : getshortestDistanceStage()
activate stageList

alt exception caught : no race
stageList --> controller : exception : no stages
controller --> UI : Result: false, errorMessage
UI -> administrator : Inform Failure
else no exception

stageList --> stageList : shortest = firstStage
stageList --> stageList : List<Stage> shortestList
stageList --> stageList : shortestList.add(shortest)
loop for every other stage in stageList
stageList -> stage : compareDistance(shortest)
activate stage

alt longer
stage --> stageList : 1
stageList --> stageList : shortest = aStage
stageList --> stageList : shortestList.clear
stageList --> stageList : shortestList.add(shortest)

else equal
stage --> stageList : 0
stageList --> stageList : shortestList.add(aStage)

else shorter
stage --> stageList : -1
deactivate stage

end
end

stageList --> stageList : stageDTOList = \ncreateStageDTOList\n(shortestList)


stageList --> race : StageDTOList
deactivate stageList

race --> raceService : StageDTOList
deactivate race

raceService --> controller : StageDTOList
deactivate raceService

controller --> UI : Result: true, StageDTOList
deactivate controller

UI -> administrator : Display List
deactivate UI
end
end

deactivate administrator
```


## 3.3. Applied Patterns

We applied the following principles:

- GRASP:
    -
    - Information expert:
        - The StageList class will be the information expert regarding the Stage Interface, holding all necessary information to perform functions regarding it.
        - Stage Interface implementations are the information expert of their attributes, using tell don't ask when comparing two Stage instances, in which it is the implementation itself that returns the result of the comparison.
        
    - Creator:
        - The StageList will be the creator of the implementations of Stage Intergace, as it also contains and records instances of it.
     
    - Controller:
        - To deal with the responsibility of receiving input from outside the system (first layer after the UI) we use a case controller.
    
    - Pure Fabrication:
        - In this user story the StageList class was used, which does not represent a business domain concept. It was created to be responsible for all operations regarding the Stages.

    - High cohesion and Low Coupling:
        - The creation of the StageList class provided low Coupling and high Cohesion, keeping one Class as the Information Expert.
          
    - Protected Variation:
        -  With regard to this US, the biggest turning point in the future would be the introduction of different types of Stage, so we implemented the Stage interface for the Stage list to save a list of Stage Interfaces.
                    
- SOLID:
    -
    - Single-responsibility principle:
        - The Single responsibility principle pattern was used in the StandardStage class, in which it only has the responsibility to store information regarding the StandardStage.
        
    - Open-closed principle, Liskov substitution principle and Dependency inversion principle:
        - We have used abstracted interfaces (implement behaviors), where the implementations can be changed and multiple implementations could be created and polymorphically substituted for each other, like in Stage interface.
    
    
## 3.4. Tests

### 3.4.1. Controller Tests

**Test 1** : **Get Shortest Distance Stage Succes** - One Stage in Race

    @Test
    void getShortestDistanceStageSuccessOneStage() {
        createRaceController.createRace(createRaceDTO);
        addStageController.addStage(addStageDTOOne); // 40.00

        List<StageDTO> list = new ArrayList<>();
        list.add(stageDTO);

        Result<List<StageDTO>> expected = Result.success(list); // 40.00

        Result<List<StageDTO>> result = getStageByDistanceController.getShortestDistanceStage();

        assertEquals(expected, result);
    }

**Test 2** : **Get Shortest Distance Stage Succes** - Two Stages in Race, but only ONE with shortest distance

    @Test
    void getShortestDistanceStageSuccessTwoStagesWithDiferentDistances() {
        createRaceController.createRace(createRaceDTO);
        addStageController.addStage(addStageDTOTwo); // 60.00
        addStageController.addStage(addStageDTOOne); // 40.00


        List<StageDTO> list = new ArrayList<>();
        list.add(stageDTO);

        Result<List<StageDTO>> expected = Result.success(list); // 40.00

        Result<List<StageDTO>> result = getStageByDistanceController.getShortestDistanceStage();

        assertEquals(expected, result);
    }

**Test 3** : **Get Shortest Distance Stage Succes** - Two Stages in Race, with same distance

    @Test
    void getShortestDistanceStageSuccessTwoStagesWithSameDistances() {
        createRaceController.createRace(createRaceDTO);
        addStageController.addStage(addStageDTOThree); // 60.00
        addStageController.addStage(addStageDTOTwo); // 60.00

        List<StageDTO> list = new ArrayList<>();
        list.add(stageDTOThree); // 60.00
        list.add(stageDTOTwo); // 60.00

        Result<List<StageDTO>> expected = Result.success(list); // 60.00; 60.00

        Result<List<StageDTO>> result = getStageByDistanceController.getShortestDistanceStage();

        assertEquals(expected, result);
    }

**Test 4** : **Get Shortest Distance Stage Succes** - Three Stages in Race, TWO with shortest distance

    @Test
    void getShortestDistanceStageSuccessThreeStagesWithTwoWithSameDistanceShortests() {
        createRaceController.createRace(createRaceDTO);
        addStageController.addStage(addStageDTOOne); // 40.00
        addStageController.addStage(addStageDTOTwo); // 60.00
        addStageController.addStage(addStageDTOFour); // 40.00

        List<StageDTO> list = new ArrayList<>();
        list.add(stageDTO); // 40.00
        list.add(stageDTOFour); // 40.00

        Result<List<StageDTO>> expected = Result.success(list); // 40.00 40.00

        Result<List<StageDTO>> result = getStageByDistanceController.getShortestDistanceStage();

        assertEquals(expected, result);
    }

**Test 5** : **Get Shortest Distance Stage Succes** - Three Stages in Race, only ONE with shortest distance

    @Test
    void getShortestDistanceStageSuccessThreeStagesWithTwoWithOneShortests() {
        createRaceController.createRace(createRaceDTO);
        addStageController.addStage(addStageDTOOne); // 40.00
        addStageController.addStage(addStageDTOTwo); // 60.00
        addStageController.addStage(addStageDTOThree); // 60.00

        List<StageDTO> list = new ArrayList<>();
        list.add(stageDTO); // 40.00

        Result<List<StageDTO>> expected = Result.success(list); // 40.00

        Result<List<StageDTO>> result = getStageByDistanceController.getShortestDistanceStage();

        assertEquals(expected, result);
    }

**Test 6** : **Get Shortest Distance Stage Unsucces** - No Stages

    @Test
    void getShortestDistanceStageUnsuccessNoStages() {
        createRaceController.createRace(createRaceDTO);
        Result<List<StageDTO>> expected = Result.failure("No stages in this race");

        Result<List<StageDTO>> result = getStageByDistanceController.getShortestDistanceStage();

        assertEquals(expected, result);
    }
    
**Test 7** : **Get Shortest Distance Stage Unsucces** - No Race

    @Test
    void getShortestDistanceStageUnsuccessNoRace() {
        Result<List<StageDTO>> expected = Result.failure("Race is not created yet!");

        Result<List<StageDTO>> result = getStageByDistanceController.getShortestDistanceStage();

        assertEquals(expected, result);
    }


## 4. Implementation

### 4.1 Method to verify if a Race is already presente in system - RaceService Class

    private void checkRace() {
        if (this.race == null)
            throw new RaceNotPresentException("Race is not created yet!");
    }
    
### 4.2 Method to check if exists Stages in Stage List - StageList class

    private void checkForStagePresence() {
        if (stages.isEmpty()) {
            throw new IllegalArgumentException("No stages in this race");
        }
    }

### 4.3 Method to get List of Stages with shortest distance in Race - StageList class

    public List<StageDTO> getShortestDistamceStage() {
        checkForStagePresence();

        Stage longest = stages.get(0);
        List<Stage> longestList = new ArrayList<>();
        longestList.add(longest);

        if (stages.size() >= 2) {
            for (int i = 1; i < stages.size(); i++) {
                int comparison = longest.compareDistance(stages.get(i));
                if (comparison == 1) { // if equals One (1) stages.get(i) is shorter than longest
                    longest = stages.get(i);
                    longestList.clear();
                    longestList.add(longest);
                } else if (comparison == 0) {
                    longestList.add(stages.get(i));
                }
            }
        }
        return createDTOList(longestList);
    }

### 4.4 Method to Compare Distance - StandardStage class

    public int compareDistance(Stage stage) {
        StandardStage otherStage = (StandardStage) stage;
        return this.distance.compareTo(otherStage.distance);
    }
    
# 5. Integration/Demonstration

The development of this US will not have an impact on the development of the others Users Stories of this Sprint.

# 6. Observations

Implementation alternatives: we could have created a service dedicated to the management of the stages that would be contained in the application. We chose this implementation because it is the responsibility of the Race to contain a list of Stages and in the future for automatic updating of the general Stadings of the race, this organization with StageList will be easier than if we use a StageService.